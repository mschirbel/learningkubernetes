# Cursos de Kubernetes

## Primeiro curso:

Pode ser visto [aqui](https://www.udemy.com/kubernetes-microservices).

## Segundo curso

Pode ser visto [aqui](https://www.udemy.com/learn-devops-on-prem-or-cloud-agnostic-kubernetes/learn/lecture/10378444#overview).

## Terceiro curso:

Pode ser visto [aqui](https://www.udemy.com/learn-devops-advanced-kubernetes-usage/learn/lecture/7985960#overview).