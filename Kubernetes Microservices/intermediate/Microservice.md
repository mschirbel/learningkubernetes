# Microservices

Agora vamos aplicar tudo o que vimos para microserviços.

## O que é microserviços?

Existem dois tipos de sistemas:

Monolíticos e Microserviços.

Monolíticos são os sistemas que são desenvolvidos em um único arquivo, como um arquivo *.war*, de Java, por exemplo. Dentro desse arquivo podemos ter diversos serviços compondo um sistema. Geralmente possuem um DB integrado, que é conectado a vários sitemas para armazenar todos. Isso tem um problema: é difícil fazer uma alteração sem encostar em sistemas adjacentes.

Microserviços é dividir essas pequenas partes do sistema em serviços específicos, independendes um dos outros. Mas conseguem se comunicar um com os outros por meio de interfaces bem definidas. Essa separação é extrema, podendo ser em até linguagens diferentes, com seus próprios workspaces e até mesmos standalones hardware, ou seja, um instância ou um container.

Outro ponto importante é saber que pequenos times funcionam melhor para microserviços, pois são bem definidos e bem específicos.

## Integration Databases

Um dos pontos mais claros e simples de microserviços que vão contra os princípios dessa arquitetura são os Integration Databases.

Integration Databases são aqueles usados por diversas aplicações, de diversos ramos do negócio. São usados em muitos sistemas monolíticos. Não são coesos, pois contém informações de diversas áreas. E não são frouxamente acoplados, ou seja, muitas fontes podem escrever e ler sobre ele.

Que isso fique claro que Microserviços não são uma "silver bullet". Mas podem se encaixar em diversos sistemas.

O que podemos ter, com microserviços são diversos *data store*. Podem ser relacionais ou não, dependendo do uso da aplicação. Isso implica que os modelos de dados estão espalhados no sistema.

O principal ponto é: encontrar um ponto para reduzir as tabelas. Quanto menor, melhor.