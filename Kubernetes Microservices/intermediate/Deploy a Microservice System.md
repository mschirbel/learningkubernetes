# Deploy a Microservice System

O que vamos usar:

1. Queue
2. API Gateway
3. Angular Webapp
4. Logs
5. Position Tracker
6. Position Simulator

Para ver as imagens, podemos entrar nesse [link](https://hub.docker.com/u/richardchesterwood).

O sistema é para uma empresa de transporte. Os veículos estão reportando as suas posições globais.
Como não temos nenhum veículo, usaremos o microserviço de Position Simulator.

Caso queira ver os códigos fonte, clique [aqui](https://github.com/DickChesterwood/k8s-fleetman).

Após o simulator gerar as posições, passaremos essas posições para o sistema de fila que será lido pelo Position Tracker. Isso passará por um API Gateway, pois o Angular precisa se comunicar com o backend e seria muito ruim deixar o JavaScript chamar o backend. Caso o backend mude teríamos que mudar o frontend chamando esses componantes. O API Gateway é o ponto único de entrada para a aplicação.

---

## Deploying the Queue

Pois parece um ótimo ponto central para começarmos.

Vamos começar de um sistema limpo. Para eliminar todos os pods/services/rs que criamos antes, podemos usar os seguinte comando:

```
kubectl delete -f pods.yaml
kubectl delete -f services.yaml
kubectl delete -f networking-tests.yaml
kubectl get all
```

Para essa parte, usaremos o arquivo *resources/microservices/services.yaml* e *resources/microservices/workloads.yaml*.

Para a queue, precisamos saber a porta que será exporta.
No ApacheMQ a porta 8161 é usada para a console administrativa e a porta 61616 é usada para mandar e receber mensagens. Não usaremos a console administrativa, pois tudo será tratado no background. Mas para treinar, vamos expor na porta 30010.

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: queue
spec:
  selector:
    matchLabels:
      app: queue
  replicas: 1
  template: # template for the pods
    metadata:
      labels:
        app: queue
    spec:
      containers:
      - name: queue
        image: richardchesterwood/k8s-fleetman-queue:release1
```

Se esse container crashar, queremos que isso reinicie o mais rápido possível. Para isso, usaremos um Deployment, pois podemos fazer um rolling deployment - por isso não usaremos um replicaset.

Também precisaremos do service relacionado para expor no browser a admin console:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: fleetman-queue

spec:
  # This defines which pods are going to be represented by this Service
  # The service becomes a network endpoint for either other services
  # or maybe external users to connect to (eg browser)
  selector:
    app: queue

  ports:
    - name: http
      port: 8161
      nodePort: 30010

    - name: endpoint
      port: 61616
      # não precisamos de um NodePort aqui pois não vamos expor essa porta para fora do cluster.

  type: NodePort
```

Agora aplique as mudanças.

```
kubectl apply -f workloads.yaml
kubectl apply -f services.yaml
kubectl get all
```

Podemos acessar a queue usando o IP do Cluster na porta 300010.

---

## Deploying Position Simulator

Seguindo o mesmo padrão da queue, temos que escrever um deployment e um service. 
Mas neste caso, teremos duas mudanças. Não usaremos portas expostas, pois o serviço é isolado, só envia dados para uma queue.
E precisaremos de uma variável de ambiente, *SPRING_PROFILES_ACTIVE=production-microservice*

Para o deployment:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: position-simulator
spec:
  selector:
    matchLabels:
      app: position-simulator
  replicas: 1
  template: # template for the pods
    metadata:
      labels:
        app: position-simulator
    spec:
      containers:
      - name: position-simulator
        image: richardchesterwood/k8s-fleetman-position-simulator:release1
        env:
        - name: SPRING_PROFILES_ACTIVE
          value: production-microservice
```

E como não teremos portas expostas, não usaremos um serviço.

Perceba que no fim do arquivo, temos uma tag para *env*.

### How to see a Pod's logs

```
kubectl logs [pod name]
```

Não existem logs de deployment, service ou replicaset. Somente de Pods.

---

## Deploying the Position Tracker

Para esse microserviço, teremos a porta 8080 exposta como uma REST interface.

Também precisa de uma env *SPRING_PROFILES_ACTIVE=production-microservice*

Para o deployment:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: position-tracker
spec:
  selector:
    matchLabels:
      app: position-tracker
  replicas: 1
  template: # template for the pods
    metadata:
      labels:
        app: position-tracker
    spec:
      containers:
      - name: position-tracker
        image: richardchesterwood/k8s-fleetman-position-tracker:release1
        env:
        - name: SPRING_PROFILES_ACTIVE
          value: production-microservice
```

Como ele precisa de uma porta exposta, precisaremos de um service:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: fleetman-position-tracker

spec:
  # This defines which pods are going to be represented by this Service
  # The service becomes a network endpoint for either other services
  # or maybe external users to connect to (eg browser)
  selector:
    app: position-tracker

  ports:
    - name: http
      port: 8080
      nodePort: 30020

  type: NodePort
```

A interface pode ser acessada usando:

**/vehicles/City%20Truck**

PS: *%20%* é como o o browser identifica espaços em branco.

Mas percebemos uma coisa. Não queremos mostrar isso para o mundo, para isso, devemos editar o serviço:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: fleetman-position-tracker

spec:
  # This defines which pods are going to be represented by this Service
  # The service becomes a network endpoint for either other services
  # or maybe external users to connect to (eg browser)
  selector:
    app: position-tracker

  ports:
    - name: http
      port: 8080

  type: ClusterIP
```

Com o ClusterIP a porta só vai ser exposta para o cluster.

---

## Deploying the API Gateway

Precisamos expor a porta 8080 com a mesma variáveis de ambiente de antes.

Para o deployment:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: api-gateway
spec:
  selector:
    matchLabels:
      app: api-gateway
  replicas: 1
  template: # template for the pods
    metadata:
      labels:
        app: api-gateway
    spec:
      containers:
      - name: api-gateway
        image: richardchesterwood/k8s-fleetman-api-gateway:release1
        env:
        - name: SPRING_PROFILES_ACTIVE
          value: production-microservice
```

Para o serviço expor a porta 8080:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: fleetman-api-gateway

spec:
  # This defines which pods are going to be represented by this Service
  # The service becomes a network endpoint for either other services
  # or maybe external users to connect to (eg browser)
  selector:
    app: api-gateway

  ports:
    - name: http
      port: 8080

  type: ClusterIP
```

---

## Deploying Webapp

Temos que expor a porta 80 e as mesmas variáveis de ambiente.

Para o deployment:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: webapp
spec:
  selector:
    matchLabels:
      app: webapp
  replicas: 1
  template: # template for the pods
    metadata:
      labels:
        app: webapp
    spec:
      containers:
      - name: webapp
        image: richardchesterwood/k8s-fleetman-webapp-angular:release1
        env:
        - name: SPRING_PROFILES_ACTIVE
          value: production-microservice
```

Para expor a porta, precisamos de um service:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: fleetman-webapp

spec:
  # This defines which pods are going to be represented by this Service
  # The service becomes a network endpoint for either other services
  # or maybe external users to connect to (eg browser)
  selector:
    app: webapp

  ports:
    - name: http
      port: 80
      nodePort: 30080

  type: NodePort
```

Não esqueça que o selector tem que bater com o label do pod.