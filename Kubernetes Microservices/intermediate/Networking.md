# Networking and Service Discovery

Imagine que fizemos dois deploys, um de uma app web e outro de um banco de dados. Como fazemos para conectar esses dois servidores?

Se fizermos isso em um único Pod, podemos conectar um ao outro usando *localhost*. 

Mas como sabemos, fazer isso é uma prática ruim. Queremos cada parte em seu microserviço, pois o Pod pode crashar ou ficar offline.

Não queremos somente em Pods diferentes, mas em Serviços diferentes. Cada um desse serviço vai ter o seu próprio internal IP, que só é visível dentro do próprio Cluster. Mas esses IPs são alocados dinamicamente. Então como vamos pegar cada um deles?

K8s mantém o seu próprio DNS service. É composto de key-value pairs. Esse serviço roda no background assim quando iniciamos nosso Cluster.

Como fazer para isso aparecer na linha de comando?

## Namespace

Quando rodamos um *kubectl get all* recebemos somente as entradas que criamos manualmente. Mas como vimos antes, temos o serviço de DNS habilitado por default. Por que então não aparece?

Por causa de **namespeaces**. Namespeaces são partiçoes dos recursos do K8s. Por exemplo, uma namespace do frontend e outra para o backend.

Até agora não usamos nenhuma namespace. E quando não especificamos nenhuma, o recurso é criado na *default*. E são esses que estão listados no *kubectl get all*

Para ver quais namespaces estão criadas:

```
kubectl get namespaces
kubectl get ns
```

Veja que existem outras duas, *kube-public* e *kube-system*.

Para ver o que tem dentro delas:

```
kubectl get all -n kube-system
kubectl get all -n kube-public
```

Na system é onde acharemos o nosso DNS.

O problema de colocar um recurso numa namespace é que temos que lembrar sempre onde ela está.

## Inside a container

Nós queremos acessar de um webapp acessar nosso banco de dados. Para isso, vamos fazer um lookup no DNS para o IP do service do database.

Para subir o webapp temos o arquivo *webapp.yaml* e para subir o database temos o arquivo *database.yaml*

Para rodar

```
kubectl apply -f webapp.yaml
kubectl apply -f database.yaml
kubectl get all
```

Vamos agora, entrar no container do DB:

```
kubectl exec -it [db pod name] sh
ls -l
```

## Winpty

Caso queira algo para resolver os problemas de visualização de shell do cygwin, use o winpty:

[Link Aqui](https://github.com/rprichard/winpty/releases/)

[Video maneiro apra instalar o cygwin](https://www.youtube.com/watch?v=hh-V6el8Oxk)

Baixe os arquivos e depois

```
cd winpty/bin/
cp * /usr/local/bin
```

Para usar:

```
winpty kubectl exec -it [db pod name] sh
ls -l
```

## Service Discovery

Como nosso webapp sabe onde achar o DNS do DNS Server? O K8s automaticamente configura os Pods para encontrarem a namespace de DNS dele mesmo. Podemos ver isso no arquivo resolv.conf:

```
cat /etc/resolv.conf
```

Esse é o arquivo que configura o DNS. Diferente do */etc/hosts* esse arquivo trabalha com nameservers, agrupamentos de linhas de DNS.

É assim que o DNS funciona dentro do K8s.

Para verificarmos isso, podemos usar o comando *nslookup*, seja para internet ou localmente

```
nslookup google.com
nslookup database
kubectl get all
```

Compare o nslookup do databse com o IP interno dele.

Para conectar a um DB em outro host, por exemplo, usando o DNS o comando seria assim:

```
mysql -h database -uroot -ppassword dbname
```

Com a flag *-h* podemos especificar um host para nos conectarmos, assim nossa conexão será resolvida pelo DNS do K8s.

## Fully Qualified Domain Name- FQDN

Na verdade o serviço não é registrado sobre um nome específico que é dado nos arquivos de configuração.

Na verdade, o DNS do K8s funciona baseado no FQDN que é um nome extenso do recurso.

Ao rodar um *nslookup database* iremos receber dois resultados, um IP e um hostname bem extenso com o nome do Cluster.

Sendo no seguinte formato: *[resource name].[namespace of resource].[type of resource].[cluster].[type of cluster]*

E qual a diferença? Para encontrarmos algo fora da namespace que estamos, precisamos usar o FQDN.

Assim, quando formos referenciar um recurso, devemos usar o FQDN, assim, não teremos erro para resolve de DNS.