# Persistence

No momento, temos 5 deployments funcionando. Todos trazem a webapp funcionando.

Agora, teremos uma release nova. Para fazer isso, temos dois arquivos *workloads.yaml* e *services.yaml* onde estão as definições para nossas imagens. Todas elas receberão uma nova tag para a imagem nova.

Basta trocar a tag para release2.

Para aplicar:

```
kubectl apply -f workloads.yaml
kubectl apply -f services.yaml
```

Em nenhum momento devemos ver um HTTP 500, somente algum pequeno lag, mas não teremos downtime propriamente dito.

No atual momento, não existe um sistema que grava as posições em disco. São mantidas, em memória, no Spring Boot. Isso causará que o sistema reinicia algumas vezes e assim, perderá dados.

Para evitar isso, teremos que fazer um *split* no serviço de position tracker em dois microserviços, um para calcular as velocidades e outro para armazenar o histórico. Logo, usaremos um BD, a escolha será o MongoDB.

Lembre que, isso não será um DB de integração, isso fere o princípio de unicidade dos microserviços.

Para subir o novo DB, temos os seguintes arquivos:

Mongo-Stack:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mongodb
spec:
  selector:
    matchLabels:
      app: mongodb
  replicas: 1
  template: # template for the pods
    metadata:
      labels:
        app: mongodb
    spec:
      containers:
      - name: mongodb
        image: mongo:3.6.5-jessie
        volumeMounts:
          - name: mongo-persistent-storage
            mountPath: /data/db
      volumes:
        - name: mongo-persistent-storage
          # pointer to the configuration of HOW we want the mount to be implemented
          persistentVolumeClaim:
            claimName: mongo-pvc
---
kind: Service
apiVersion: v1
metadata:
  name: fleetman-mongodb
spec:
  selector:
    app: mongodb
  ports:
    - name: mongoport
      port: 27017
  type: ClusterIP
```

Nesse arquivo subimos uma imagem oficial do mongo, com um serviço para habilitar a porta default.

Mas perceba que existe uma tag de volume no Deployment, **volumes**. Trataremos disso depois.

Agora, precisamos tratar a comunicação dos microserviços com o Mongo. Para isso, precisaremos de um service, assim como mostrado no arquivo acima. A única parte necessária, é o nome, dentro de metadata. Deve ser fleetman-mongodb, pois isso está dentro do arquivo *k8s-fleetman-position-tracker/src/main/resources/application-production-micrservice.proprieties*, disponível no [github](https://github.com/DickChesterwood/k8s-fleetman/).

Agora, vamos subir a nova release, que vai se comunicar com o banco de dados. Basta trocar a tag da image para release3, nos arquivos de workload.

## Volumes

Isso foi necessário, pois todos os dados gravados no DB serão deixados no FS do container. Isso é um problema, pois caso seja desfeito o container, perderemos todos os dados.

Para isso, teremos que ter um storage para nosso DB. É exatamente isso que temos na tag:

```yaml
volumes:
        - name: mongo-persistent-storage
          # pointer to the configuration of HOW we want the mount to be implemented
          persistentVolumeClaim:
            claimName: mongo-pvc
```

Agora, precisamos fazer a persistência dos dados.

Para entender os conceitos, acesse a [documentação](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)

Existem dois conceitos fundamentais a serem entendidos:

1. PersistentVolume(PV)
2. PersistentVolumeClaim(PVC)

A persistência no k8s é bem inteligente, por isso é complexa. Para isso, queremos que os nossos dados sejam armazenados fora do nosso Cluster, pois caso aconteça com ele, não perderemos os dados.

Na AWS temos o EBS - Elastic Block Store, que é um HDD ou SDD. Nele podemos guardar os dados do nosso DB e acessar eles dos nossos deployments.

Precisamos declar dentro do nosso container um Mount de Volumes:

```yaml
  spec:
      containers:
      - name: mongodb
        image: mongo:3.6.5-jessie
        volumeMounts:
          - name: mongo-persistent-storage
            mountPath: /data/db
```

Sendo */data/db* a pasta que o MongoDB usa para guardar os dados.

Precisamos dizer para o K8s **o que** queremos configurar e **como** queremos configurar.

Isso é bom porque caso desejemos mudar a infraestrutura de lugar, não nos preocupamos com tudo, somente com uma das partes.

Temos um exemplo disso no arquivo *resources/persistence/storage.yaml*.

O PVC seria a ideia, e essa ideia pode ser aplicada para diversos Pods. Um PV seria a implementação dessa ideia.

PVC tem um nome, o qual será usado para referenciar outros arquivos, que por sinal está no arquivo *resources/persistence/storage.yaml*.

Dentro do PVC, temos que referenciar **como** faremos esse storage, nisso usamos a tag:

```yaml
spec:
  storageClassName: mylocalstorage
```

Com essa tag, declaramos nosso PV:

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: local-storage
spec:
  storageClassName: mylocalstorage
  capacity:
    storage: 20Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/mnt/some new/directory/structure/"
    type: DirectoryOrCreate
```

Para linkar um PVC com um PV usamos a storageClassName. Agora, nosso PVC vai procurar por PVs que:

1. tenham o mesmo nome de classe
2. tenham uma capacidade maior ou igual a necessária do PVC
3. tenham o accessMode correto

Se usarmos um *kubectl get all* não veremos os volumes sendo listados. Isso ocorre porque o all representa workloads, não recursos. Para isso:

```
kubectl get pv
kubectl get pvc
```

pv significa persistent volumes.
pvc significa persistent volumes claim.