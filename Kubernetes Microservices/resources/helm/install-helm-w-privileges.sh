#!/bin/bash

echo "wget na versao mais recente"

wget https://storage.googleapis.com/kubernetes-helm/helm-v2.13.1-linux-amd64.tar.gz

echo "tar nesse file"

tar -xzfv helm-v2.13.1-linux-amd64.tar.gz

echo "joga no path"

sudo mv linux-amd64/helm /usr/local/bin

echo "remove o tar"

rm -rf helm-v2.13.1-linux-amd64.tar.gz

echo "helm init"

helm init

echo "ve se o pod tiller ta online"

kubectl get pods -n kube-system | grep "tiller"

echo "helm version, se deu certo, td beleza"

helm version

echo "update no repo do helm"

helm repo update

echo "os comandos a seguir corrigem as permissoes do tiller pod:"

kubectl create serviceaccount --namespace kube-system tiller

kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller

kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}' 