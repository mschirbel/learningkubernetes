## Examples

Caso queira ver os exemplos **locais** mais recentes, use os arquivos em *resources/persistence/*

Caso queira ver os exemplos **em nuvem** mais recentes, us os arquivos em *resources/cloud/*

### Ordem das anotações

1. Basics
2. Intermediate
3. Cloud
4. Logging
5. Monitoring
6. Alert Manager

### Ordem dos recursos

1. first-files
2. replicasets
3. deployments
4. active-mq
5. networking
6. microservices
7. persistence
8. aws
9. logging
10. monitoring
11. helm
12. alert

## On GCP

Podemos ver o vídeo [aqui](https://www.youtube.com/watch?v=w9vy3wHGKeo)

## Install Minikube on Windows

Used this tutorial [here](https://medium.com/@JockDaRock/minikube-on-windows-10-with-hyper-v-6ef0f4dc158c)

The command to start our minikube:

```
minikube start --vm-driver hyperv --hyperv-virtual-switch "Primary Virtual Switch"
```