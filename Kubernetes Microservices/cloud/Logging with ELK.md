# Elastic Stack

## Logging

Ainda estamos fazendo tudo na AWS.:

```
kubectl get pods
```

Cada um dos nossos pods, possuem containers. E cada container escreve uma log.

E podemos ver as logs de um pod específico:

```
kubectl log [pod-name]
```

Quando estamos trabalhando localmente fica fácil analisar as logs.
Mas ao fazer o deploy para a nuvem precisamos fazer uma análise de logs em *real-time* para faz analytics de nosso tráfego.

Mas as logs podem capturar quaisquer exceptions e warnings.

Um dos problemas que tenho, é que os Pods são efêmeros, pois caso o Pod morra, perderemos os arquivos de logs.

## Componentes

A solução mais comum é a ELK Stack. É chamado de Stack, porque usaremos diversos componentes para nosso componente de Logging.

Imagine que temos dois containeres dentro de um Pod, ambos escrevendo logs para fazermos a análise. Para isso, usaremos o **Logstash ou Fluentd**. Esse componente vai pegar todas as logs do nosso Pod. Mas não vai armazenar e nem analisar os dados

Depois, usaremos o **ElasticSearch**, é um motor de análise, seletor distribuído de logs. Esse componente será usado para analisar e armazenar.

Para visualizar os dados, usaremos o **Kibana**. Podemos montar gráficos, dashboards e tudo mais.

## Arquitetura

Instalaremos Fluentd em todos os nodes.
ElasticSearch estará no mínimo em dois nodes
Kibana estará em somente 1 Node.

Agora só precisamos encontrar as imagens Docker para cada um dos componentes.

### Fluentd

Podemos encontrar o fluentd já configurado [no repositório oficial do kubernetes](https://github.com/kubernetes/kubernetes/tree/master/cluster/addons/fluentd-elasticsearch).

Esse repo tem uma pasta de addons que é mantido pelo time do k8s. No link acima, temos a configuração de tudo que precisamos. Existe um par de arquivos para cada um dos componentes citados acima. Dois para o ElasticSearch, dois para o Fluentd e dois para o Kibana.

Mas temos nas pasta *resource/logging/* dois arquivos, com os pares já combinados. Ficando assim, mais fácil para todo mundo. Tem também um arquivo de configuração do fluentd.

Basta agora, aplicar esses arquivos no nosso cluster.

### Tunning the config files

No arquivo *resources/logging/elastic-stack.yaml* temos dois novos tipos de configurações:

1. ClusterRole
2. ServiceAccount
3. DaemonSet
4. StatefulSet
5. ConfigMap

Isso da algumas permissões ao Fluentd para acessar o seu cluster.

DaemonSet é parecido com um RS ou um Deployment mas não precisamos especificar um número de replicas, ele sabe que deve rodar em **todos** os nodes.

StatefulSet também é um tipo de RS mas a diferença é que os Pods não terão um nome aleatório, sempre terão o nome declarado + "-[numero]", esse número vai de 0 até o quando for necessário.

ConfigMap cria uma série de key-value pairs que podem ser acessados por todo o cluster.

Agora, vamos aplicar os dois arquivos

```
kubectl apply -f fluentd-config.yaml
kubectl apply -f elastic-stack.yaml
kubectl get pods
```

Você não verá que foi nada criado, isso acontece porque o deploy foi realizado em outra namespace:

```
kubectl get pods -n kube-system
kubectl get all -n kube-system
```

Veja que o service do Kibana tem um IP Externo:

```
kubectl describe svc kibana-logging -n kube-system
```

Com esse LoabBalancer Ingress e a porta destinada, podemos acessar as dashboards do Kibana.

## Kibana

Nesse curso, vamos trabalhar com logs. Devemos usar alguns Index Patterns.

Esse index será o usado pelo ElasticSearch na análise das logs. Ao usar o Fluentd ele cria índices nas logs começancom com Logstash-[dia da coleta da log]. Assim podemos saber quais logs devemos analisar no ElasticSearch e assim visualizar o Kibana.

No pattern, vamos usar um wildcard:

```
logstash-*
```

Isso servirá para encontrar quaisquer indices que começem com logstash.

No próximo passo, devemos usar um TimeField, por default temos o @timestamp criado pelo Fluentd.

Após criar, veremos parâmetros das logs dos nossos Pods.

Clicamos no link Discover, no menu da esquerda e veremos um Search Engine. Nessa página estamos vendo a log de **tudo**.

Estamos interessados nos erros. Ao pesquisar por *error*, podemos ver que ocorreram alguns erros.

Para escolher o tempo de coleta/visualizalção das logs:

![](../resources/images/Anotação-2019-03-28-211854.jpg)

Ao clicar no dropdown de um *hit* podemos ver os detalhes e todos os metadados coletados pelo Fluentd.

### Filters and Refreshes

Ao ver os detalhes de um erro, podemos ver que ele pega as informações de namespace do Kubernetes. Isso pode ser útil ao criar filtros.

Para criar um filtro, há um botão logo abaixo da barra de pesquisa. Adicione o kubernetes.namespace-name. Perceba que temos a kube-system e a default. Seleciona default que é a nossa app.

### System Failure Analysis

Vamos simular uma grave falta na queue. Trocamos as replicas do queue para 0. E agora vamos aplicar essa mudança:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: queue
spec:
  selector:
    matchLabels:
      app: queue
  replicas: 0 # trocado para ter erro.
  template: # template for the pods
    metadata:
```

E agora aplique:

```
kubectl apply -f workloads.yaml
```

Veja na aplicação que a aplicação parou. No Kibana, troque o período para os últimos 15min. E veremos que o gráfico mostrará um pico de erros.

Nos erros, vemos uma mensagem clara de "Queue Unavailable".

### Dashboards

Podemos criar dashboards e alarmes com Kibana. Podemos clicar em Save, e o Kibana vai guardar o termo de nossa pesquisa e os filtros que deixamos.

Ao clicarmos no menu Visualize e em Create Visualization.

Podemos escolher diversos graficos, tabelas, mapas e time series.

Vamos demonstrar o Gauge. Clique e escolha a saved search que criamos. É como um velocimetro. E podemos editar as options desse gráfico. Para salvar as opções, clicamos no Play Button(Apply Changes). 

Agora, podemos ir no menu e clicar em Dashboard e em Add. Escolhemos a nossa visualization e podemos editar essa visualização na tela.