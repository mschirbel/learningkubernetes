# Alert Manager

A parte do Prometheus e do Grafana, temos um pod para o AlertManager:

```
kubectl get pods -n monitoring
```

AlertManager é um plugin que instala-se no Prometheus para que, caso alguma métrica passem de tresholds, um alerta é disparado.

Para visitar essa aplicação:

```
kubectl get svc -n monitoring
```

Veja que existe um *alermanager-operated*. Está na porta 9093.
Podemos visitar também o *kube-prometheus-alertmanager*

Ao invés de colocar um LoadBalancer, podemos acessar de outra forma. Para isso, usaremos um proxy, acessaremos de fora do Cluster.

## Access on our API Gateway LB

Ao usar o DNS do API Gateway no browser, seremos perguntados por um user e senha.

O user é admin. Para descobrir a senha:

```
kubectl config view --minify
```

Isso dará a senha. E teremos todo os endpoints de nossa aplicação em formato JSON.

Aqui, devemos encontrar a URI para acessar o serviço do *alermanager-operated*. Para isso, acesse:

```
<URL DO ELB>/api/v1/namespaces/monitoring/services/alertmanager-operated:9093/proxy
```

Não é uma URL bonita, pois é feita para que os sysadmins tenham acesso, somente com a senha retirada de dentro do Cluster.

OBS: não esqueça que deve ser substituído o nome da namespace.

## AlertManager

Deve haver pelo menos 1 alerta. Temos um alerta de "watch dog" que é feito para que o pipeline de alertas funciona.

O alerta vai parar quando a situação for normalizada e o Prometheus verificar que normalizou. E só vai existir um único alerta até que seja resolvido. Exceto o "watch dog", ele sempre fica online.

Temos que redirecionar os alertas para algo que seja informativo. No nosso caso, para o Slack Channel.

## Slack

Slack é uma plataforma para comunicação na empresa.

Após criar uma conta, crie um canal para cuidar dos alertas.

Agora, criamos um App e procura por *Incoming WebHooks*, isso permite que enviemos dados para o Slack por meio de uma REST interface.

Clique em *Add Configuration* e selecione o canal que você criou.

Nos foi dados uma URL para esse webhook. Na pasta *resources/alert/* temos o arquivo *alertmanager.yaml* e nesse arquivo devemos inserir essa URL.

Cuidado com essa URL, **não publique** essa URL. **Nunca**.

Agora precisamos direcionar o AlertManager para o Slack.

## Configuring the Alert Manager

No arquivo em *resources/alert/* vemos as seguintes configurações:

```yaml
global:
  slack_api_url: '<<add your slack endpoint here>>'
route:
  group_by: ['alertname']
  group_wait: 5s
  group_interval: 1m
  repeat_interval: 10m
  receiver: 'slack'

receivers:
- name: 'slack'
  slack_configs:
  - channel: '#alerts'
    icon_emoji: ':bell:'
    send_resolved: true
    text: "<!channel> \nsummary: {{ .CommonAnnotations.message }}\n"
```

Primeiro instanciamos uma variável global, que será o link providenciado pelo Slack.

Depois, agrupamos todos os alertas que possuem o mesmo 'alertname'. Ou seja, todos os alertas com o mesmo nome, vão para o mesmo receiver, *slack*.

O group_interval vai recolher todos os grupos dentro do período de tempo.

group_wait é um parâmetro para esperar após o alerta chegar. Vai que é um glitch resolvido instantaneamente. Nao vamos querer alertas disso.

Na última parte, configuramos para onde enviar.

## Config with a Secret

Agora, pegue esse arquivo e **deve** ter o nome de *alertmanager.yaml*.

Não esqueça de trocar a url entre ''.

Precisamos agora, ver as logs do pod *alertmanager-kube-prometheus-0*

```
kubectl logs -n monitoring alertmanager-kube-prometheus-0
```

Teremos um erro, pois esse Pod tem dois containers. Para ver as logs, precisamos escolher o container:

```
kubectl logs -n monitoring alertmanager-kube-prometheus-0 -c alertmanager
```

O que queremos ver é quando a config for alterada.

Para aplicar esse yaml, precisamos criar um *secret*. É um jeito de colocar configurações de tal forma que não serão mostradas em logs. Não são, necessariamente, encriptados.

```
kubectl get secret -n monitoring
```

Veja que o alertmanager já tem um secret, *alertmanager-monitoring-prometheus-oper-alertmanager*

```
kubectl get secret -n monitoring alertmanager-monitoring-prometheus-oper-alertmanager -o json
```

Veja que temos uma tag de alertmanager.yaml que está em b64. O que queremos ver é que ao trocarmos o nosso arquivo, essa tag irá alterar o seu conteúdo.

Caso queira ver o seu conteúdo:

```sh
kubectl get secret -n monitoring alertmanager-monitoring-prometheus-oper-alertmanager -o json | grep "alertmanager.yaml"

echo "#conteudo do grep acima" | base64 -d
```

Precisamos deletar esse secret:

```
kubectl secret delete alertmanager-monitoring-prometheus-oper-alertmanager -n monitoring
```

Agora, precisamos recriá-lo:

```
kubectl create secret -n monitoring generic alertmanager-monitoring-prometheus-oper-alertmanager --from-file=alertmanager.yaml
```

Agora se olharmos as logs:

```
kubectl logs -n monitoring alertmanager-kube-prometheus-0 -c alertmanager -f
```

#### Error on etcd with Master's firewall.

No Slack temos um erro de *InsufficientMembers*. Isso acontece porque o Kops configura o nosso Cluster com um firewall no MasterNode. E como o Prometheus está instalado em um Worker, ele acaba não acessando o Master.

Isso ocorre porque no SG do MasterNode estão liberadas as portas de 4003-65535. Mas o Prometheus também usa a porta 4001 e 4002. Então vamos fazer isso manualmente.

#### T2 Instances

As instâncias começando por t2 são instâncias de Burst.

Isso significa que não utilizar a cpu, ou seja, ficar idle, acumula créditos para aquela instância.

Mas se ela tiver um trabalho constante, pode ser que a longo prazo ela venha a ter um alto consumo de CPU pois acabaram os créditos.

Isso pode ser visto na aba Monitoring da dashboards de EC2 na AWS console.