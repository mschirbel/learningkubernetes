# Monitoring

Using Prometheus and Grafana.

Dependendo do cloud vendor, temos monitoração habilitada por default. Ao selecionar uma instância, temos uma aba de monitoração.

Podemos ver a CPU, Disk Reads, Disk Writes, Network In e etc.

Na AWS temos o serviço de CloudWatch que faz essa monitoração. De graça, temos apenas o básico. Mas podemos habilitar a monitoração detalhada. 

O problema dessa monitoração é que é feito pela AWS. Poderíamos querer algo nosso. Para isso, usaremos o *Prometheus* e *Grafana*.

*Prometheus* é uma solução open-source de monitoração, da mesma empresa que administra o Kubernetes.

Prometheus é uma ferramenta de monitoração, mas ela não é tão boa para exibir gráficos. Para isso, vamos usar o Grafana.

Para instalar o *Prometheus* e *Grafana* pode ser mais fácil fazer isso usando o Helm.

Helm é um package manager para Kubernetes. Podemos ver [nesse link](https://github.com/helm/helm).

## Helm

Helm permite instalar uma aplicação de Kubernetes apenas com a linha de comando.

É com ele que vamos instalar o Prometheus e Grafana.

Para isso, vamos na instância que subimos na AWS e vamos instalar o Helm lá. Precisamos de um Helm Pod para comunicar com o nosso cluster.

No link acima, temos uma parte de Install, nessa parte temos um link para o release.

Temos um scrip com os passos em *resources/helm/fix-helm-priviledges.sh*

Primeiro baixamos os binários e jogamos o binário do helm no path.
Depois, usamos o comando

```
helm init
```

Para iniciar um Pod com helm

Agora, vamos ver o pod criado:

```
kubectl get pods -n kube-system
```

Vemos que há um **tiller** pod criado.

Se deu certo, rode um 
```
helm version
```

Agora, vamos na parte de charts do helm, clicando [aqui](https://github.com/helm/charts/tree/master/stable)

Na maioria dos charts, temos várias configurações. Essas configurações são as default do helm. E podemos editá-las.

### Installing a Chart

Antes de instalar, use um update do repositório:

```
helm repo update
```

E agora instale:

```
helm install stable/mysql --name my-special-mysql --set mysqlPassword=password
```

Perceba que, para redefinir uma configuração default, temos que usar o parâmetro *--set* seguido do nome da configuração decrito no GitHub daquele chart.

Nesse exemplo, estamos instalando o MySQL, por isso, devemos colocar uma senha.

Mas rodar esse comando, é que o nosso Pod Tiller não aceita a namespace default pois essa namespace não tem acesso a kube-system, que é uma adm do cluster.

Para fazer um override:

```sh
kubectl create serviceaccount --namespace kube-system tiller

kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller

kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}' 
```

Para deletar um chart:

```
helm delete --purge my-special-mysql
helm ls
```

## Install Prometheus and Grafana Charts

### Prometheus

Temos [aqui](https://github.com/helm/charts/tree/master/stable) uma página com as aplicações estáveis do helm.

Podemos caçar aqui a pasta de cada um dos componentes que queremos.
Nesse curso, vamos usar o [**prometheus-operator**](https://github.com/helm/charts/tree/master/stable/prometheus-operator).

Esse [projeto](https://github.com/helm/charts/tree/master/stable/prometheus-operator) combinaram Prometheus, Alert Managar e Grafana. Para que seja uma solução completa de monitoração.

Para instalar:

```
helm install --name monitoring --namespace monitoring stable/prometheus-operator
kubectl get all -n monitoring
```

Há um front-end para o prometheus, sendo este o motor necessário por coletar os dados dos nodes. Mas mesmo assim, podemos ver o front-end dele.

Tem um service chamado *monitoring-prometheus-oper-prometheus*. Esse é o do front-end.

Para editar de ClusterIP para Loadbalancer:

```
kubectl edit -n monitoring service/monitoring-prometheus-oper-prometheus
```

Agora, basta editar de ClusterIP para LoadBalancer dentro do *type*. E poderemos acessar o ELB do Prometheus.

### Grafana

Para visitarmos a interface web do Grafana:

```
kubectl get all -n monitoring
```

Tem um service chamado *monitoring-grafana*. Esse é o do front-end do Grafana.

Para editar de ClusterIP para Loadbalancer:

```
kubectl edit -n monitoring service/monitoring-grafana
```

Agora, basta editar de ClusterIP para LoadBalancer dentro do *type*. E poderemos acessar o ELB do Grafana.

Esse frontend roda na porta 80.

Para acessar a interface web, temos que usar o usuário **admin** e a senha **prom-operator**. Isso podemos encontrar na seção de configuração do [prometheus-operator](https://github.com/helm/charts/tree/master/stable/prometheus-operator).

O próximo passo é adicionar usuários, não admins, para o sistema.

Podemos escolher algumas dashboards padrão, out até mesmo criar a nossa própria.