# Moving to Cloud

Como vendor, usaremos AWS, mas K8s pode ser usado para Azure, AWS ou GCP.

Com k8s podemos automatizar muitos processos na nuvem, apenas lendo os arquivos que já construimos.

Não precisaremos criar nenhum EC2, LB, AutoScaling. O k8s se encarrega de criar tudo para nós.

## Nodes

Quando formos fazer o deploy na nuvem, vamos criar diversos nodes. Nodes são computadores que conteram um microserviço, em média. Em K8s teremos vários nodes, em Cluster.

A primeira vantagem é que os nodes não precisam ser grandes e potentes. Até porque conteram pequenas partes do sistema dentro deles.

A outra vantagem, é que teremos o Master Node que vai fazer o agendamento dos outros Nodes.

Caso exista um microserviço que seja essencial, podemos replicar um serviço, para que tenha um failover e eliminamos o downtime dentro do Master Node.

## Kops installing

Primeiramente, logue na conta da AWS e escolha uma region de preferência.

Nosso primeiro passo, será instalar o K8s em nossa conta. Para deixar isso mais fácil, podemos usar o **Kops**.

Kops significa Kubernetes Operations. Isso é totalmente diferente do minikube, ou seja, é totalmente voltado para produção.

Kops foi originalmente feito para AWS, mas agora tem suporte para Azure e GCP.

Para tornar tudo mais fácil, crie uma instância EC2, com Linux. Pode ser uma micro, sem problemas. Nessa máquinas iremos instalar o kops, apenas para controlar as outras instâncias. O Kops se conecta com a sua conta e vai subir o que for necessário.

Para essa instância, devemos configurar um SG especial, devemos deixar habilitado somente o nosso IP para acesso SSH:

![](../resources/images/Anotação-2019-03-23-203947.jpg)

Crie uma key pair e baixe para usarmos ao logar na instância. Essa será a única instância que criaremos na mão. O restante, vamos deixar o Kops cuidar.

Para isso, temos um script em anexo em *resources/aws/install-kops.sh*.

Ao digitar o comando, deveremos ver o binário instalado no path:

```
kops
```

Agora, precisamos instalar o kubectl também. Se você rodou o script, provavelmente estará lá também.

Agora, precisamos configurar um usuário Kops na AWS com as certas permissões. Isso fica mais fácil com a CLI da AWS.

Veja que no script fizemos isso, instalamos o Kops, depois o kubectl, depois a CLI AWS e por último, demos as permissões necessárias.

O Kops precisa das seguintes permissões:

1. AmazonEC2FullAccess
2. AmazonRoute53FullAccess
3. AmazonS3FullAccess
4. IAMFullAccess
5. AmazonVPCFullAccess

Ao rodar o script, teremos tudo lá. Ao rodar ele, grave a Access Key, mas a Secret Access Key **nunca** deve ser revelada.

Agora temos que configurar a nossa conta:

```
aws configure
aws iam list-users
```

Também temos isso no script.

---

## Configuring the Cluster

Agora precisamos configurar um bucket para guardarmos o estado do Cluster. É recomendado seguir:

```
aws s3api create-bucket \
    --bucket prefix-example-com-state-store \
    --region us-east-1
aws s3api put-bucket-versioning --bucket prefix-example-com-state-store  --versioning-configuration Status=Enabled
```

Lembre que o nome do bucket deve ser único.

Agora, para configurar o cluster, precisamos exportar duas variáveis de ambiente:

```sh
export NAME=myfirstcluster.k8s.local
export KOPS_STATE_STORE=s3://prefix-example-com-state-store
```

Agora, vamos ver quais AZ estão disponíveis para nós:

```
aws ec2 describe-availability-zones --region us-west-2
```

E depois, devemos configurar o cluster para operar naquela AZ:

```sh
kops create cluster \
    --zones us-west-2a \
    ${NAME}
```

No parâmetro, devemos usar a AZ que estiver disponível para nós.

Esse comando não vai criar um cluster, vai criar as configurações de um.

Pode acontecer um erro de SSH Public Key, para corrigir isso, devemos gerar uma public key e adicionar na configuração

Para adicionar:

```sh
ssh-keygen -b 2048 -t rsa -f ~/.ssh/id_rsa
kops create secret --name ${NAME} sshpublickey admin -i ~/.ssh/id_rsa.pub
```

## Editar o Cluster

Para editar o cluster:

```sh
kops edit cluster ${NAME}
```

Esse editar vai usar o VI, ou seja, bem tranquilo pra usar.

Podemos editar o instance group, ou seja, dos nodes:

```sh
kops edit ig nodes --name ${NAME}
```

ig = instance group. 

Há um grupo para os nodes e outro para o Master.

Esse arquivo vai dizer qual o tipo de instância usaremos para o cluster, bem como as subnetes e qual imagem.

Para ver as informações dos grupos:

```sh
kops get ig --name ${NAME}
```

É bem normal ter somente 1 Master. Mesmo que seja perigoso que ele venha a falhar.

## Run the cluster

Agora que está tudo está configurado, podemos fazer o apply dos arquivos e criar no hardware:

```
kops update cluster ${NAME} --yes
```

Agora é quando Kops vai criar os recursos em nossa conta da AWS.

Para sabermos se está tudo válido:

```
kops validate cluster
kubectl get nodes --show-labels
```

Espere um pouco, pois podemos ter um certo delay na criação. Pode demorar de 3 a 5 minutos.

Podemos ter algum erro de nodes não fizeram o join no cluster, basta esperara um pouco. Temos que esperar todos(nodes e master) ficarem prontos.

Veja que também foi criado um LoadBalancer, é um jeito de direcionar o tráfego para instâncias, e ele tem um *stable DNS*. Isso será usado para chamar o LB no comando *kubectl get all*. Esse LB está apontado para o Master. Caso o Master falhe, o K8s vai subir um novo e manter o mesmo DNS, ou seja, tudo fica estável.

Outro fato interessante, é que as instâncias estão dentro de um AutoScaling Group. Um para os nodes e outro para o Master.

## SSD Drives

Agora temos que editar o nosso storage para o Mongo. Podemos ver o arquivo completo em *resources/aws/storage-aws.yaml*.

Jogue todos os arquivos que fizemos, e substitua o storage pelo novo, feito para a AWS. Perceba que no novo arquivo, não declaramos no PV o quanto queremos, isso porquê o disco é alocado dinamicamente, logo, ele vai obedecer o PVC.

Para verificar se funcionou:

```
kubectl get pvc
kubectl get pv
```

Você **deve** ver o status de *Bound*

Perceba que existe uma Reclaim Policy, se estiver em *Delete* significa que ao deletar o cluster, o disco será apagado também.

## Deploy the Workload.

Após colocar todos os arquivos dentro de nossa instância EC2, vamos rodar os comandos:

```
kubectl apply -f storage-aws.yaml
kubectl apply -f mongo-stack.yaml
kubectl apply -f services.yaml
kubectl apply -f workloads.yaml
```

Todos eles estão em *resources/aws/* 

Agora, deveremos ter 2 LoadBalancers online em nossa AWS. Isso acontece porque temos uma para a API do K8s e o outro é para o webapp.

## Set a DNS Name

Caso você tenha um domínio, é possível registrar o DNS do LB da aplicação para o seu domínio. Basta entrar no serviço **Route53**. 

Clique em Hosted Zones:

![](../resources/images/Anotação-2019-03-26-205726.jpg)

Clique no seu domínio e Crie um Record Set.
Coloque o nome, o tipo, selecione Yes para Alias e selecione o LB da sua aplicação:

![](../resources/images/Anotação-2019-03-26-205933.jpg)

Assim, poderemos entrar diretamente no nosso domínio.

## Node Failure

Mesmo no evento de um node cair, o website deve permanecer disponível. Isso pode acontecer, caso uma AZ não esteja disponível. Não importa se os reports parem de chegar, desde que fiquem armazenados para que o serviço possa atualizar depois de alguns minutos.

Use o comando

```
kubectl get pods -o wide
```

Assim teremos o *wide-output* do comando, no fim teremos um IP de qual node está cada pod. Pelo IP podemos identificar qual instância está com qual microserviço. O IP é o Private DNS que podemos ver na console da AWS.

Existem microserviços que estão na mesma instância. Caso alguns serviços fiquem indisponíveis, o webapp deve permanecer acessível.

Para evitar um certo "downtime" ou uma espera longa do usuário. Podemos editar nosso arquivo workloads:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: webapp
spec:
  selector:
    matchLabels:
      app: webapp
  replicas: 3 # upgraded for avoid downtime
  template: # template for the pods
    metadata:
      labels:
        app: webapp
    spec:
      containers:
      - name: webapp
        image: richardchesterwood/k8s-fleetman-webapp-angular:release3
        env:
        - name: SPRING_PROFILES_ACTIVE
          value: production-microservice

```

Com mais replicas, reduzimos a chance de termos essa espera do usuário

```
kubectl apply -f workloads.yaml
kubectl get pods -o wide
```

## Stateless Pods

Temos um problema com os stats dos Pods. Pois a maioria deles são *stateful* ou seja, possuem dados trafegando entre eles. Caso seja feita uma alteração, podemos ter divergências nos dados.

Para isso, precisamos torná-los *stateless*. Isso tem algumas soluções viáveis, a melhor, talvez, seja usar serviços do cloud vendor.

## Deleting Cluster

Vá na instância onde está instalado o kops e onde estão os arquivos do k8s:

```
kops delete cluster --name ${NAME} --yes
```

Caso você tenha deslogado da instância, talvez seja necessário exportar novamente a variável de ambiente.

Depois delete a instância que você subiu manualmente.

## Restart the Cluster

É necessário fazer o export das variáveis de ambiente:

```sh
export NAME=myfirstcluster.k8s.local
export KOPS_STATE_STORE=s3://prefix-example-com-state-store
```

E agora criar o cluster:

```sh
kops create cluster \
    --zones us-west-2a \
    ${NAME}
```

Coloque os arquivos na instância e rode o comando:

```
kubectl apply -f .
```