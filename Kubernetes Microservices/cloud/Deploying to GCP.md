# Deploy to Google Cloud Plataform

Para essa parte, use os arquivos em *resources/aws/*. Assim manteremos as mesmas configurações.

A única alteração que teremos que fazer é no arquivo *resources/aws/storage-aws.yaml*

Dessa vez, utilizaremos o provider da GCP:

```yaml
# What do want?
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mongo-pvc
spec:
  storageClassName: cloud-ssd
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 7Gi
---
# How do we want it implemented
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: cloud-ssd
provisioner: kubernetes.io/gce-pd # google compute engine - persistent drive
parameters:
  type: pd-standard # magnetic, podemos usar o pd-ssd

```

Ao entrarmos na console da GCP criaremos um projeto e assimilaremos uma fatura para esse projeto.

Agora, iremos para o Kubernetes Engine:

![](../resources/images/Anotação-2019-03-28-192714.jpg)

Clicaremos em Cluster. Crie um Cluster, isso será o mesmo processo que fizemos no Kops. Na GCP não precisaremos rodar comandos manuais, podemos clicar em certos botões.

Dê um nome, uma descrição e marque a Location como Regional, para ser distribuido em múltiplas zonas.

O Wizard é bem interativo, não tem muito segredo. Basta clicar em Criar, isso pode demorar um pouco.

Isso dará um botão Connect, onde teremos um terminal no browser para conectar nas instâncias. Mas podemos escolhar usar o nosso terminal, mesmo assim. Basta rodar o comando que eles mesmo colam para você.

```
kubectl get all
```

Mostrará tudo o que temos agora, só temos o default service do k8s.

Temos um painel na esquerda, com o que podemos fazer o deploy:

![](../resources/images/Anotação-2019-03-28-194424.jpg)

Perceba que é o mesmo tipo de arquivos que estamos trabalhando, basta clicar em Deploy ou em Service, Storage e gerar um YAML e colar o que fizemos.

Ou podemos copiar e colar para o terminal. Ou até mesmo colocar em um repositório e clonar do repo oficial.

Para o arquivo de storage, temos o *resources/aws/storage-gcp.yaml*

Na GCP ganhamos um IP estático para o LoadBalancer. Ao clicar no link, devemos ver a aplicação abrindo.