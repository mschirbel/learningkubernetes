# Setting up Local Environment

Para usar Kubernetes localmente, devemos usar o minikube. Isso pode ser um pouco difícil de instalar.

Primeiramente precisaremos habilitar o módulo de Virtualização na BIOS.
Depois precisamos instalar um Hypervisor, em Unix podemos usar VirtualBox, em Windows podemos usar Hyper-V

Depois precisamos instalar o binário do K8s, o kubectl - kube control.
Agora, instale o binário do minikube.

Tanto o kubectl quanto o minikube, devem estar no path - /usr/local/bin.

```
sudo install minikube /usr/local/bin/
sudo mv kubectl /usr/local/bin/
```

Para habilitar o Hyper-V, podemos usar o tutorial [aqui](https://docs.microsoft.com/pt-br/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v)

Ao fazer o download, para iniciar o Minikube:

```
minikube start
```

Se estiver no Windows, deixe os executáveis no Path e execute:

```
minikube start --vm-driver=hyperv --hyperv-virtual-switch="switch"
```
*onde switch é nome para o switch criado no hyper-v*

## Possible TroubleShooting

```
minikube start --bootstrapper=localkube
```

Outro:

```
minikube stop
minikube delete
```

Remova as duas pastas .minikube e .kube do diretório do seu usuário

```
minikube start
```


## Environment in Windows with Hyper-V

First, we are using Vagrant to create our machine. Here is the Vagrantfile:

```vagrantfile
Vagrant.configure("2") do |config|
  servers=[
    {
      :hostname => "wbsv1",
      :box => "centos/7",
      :ip => "192.168.56.101",
      :ssh_port => '2210'
    }

  ]

  servers.each do |machine|

    config.vm.define machine[:hostname] do |node|
      node.vm.box = machine[:box]
      node.vm.hostname = machine[:hostname]
    
      node.vm.network :private_network, ip: machine[:ip]
      node.vm.network "forwarded_port", guest: 22, host: machine[:ssh_port], id: "ssh"

      node.vm.provider :virtualbox do |v|
        v.customize ["modifyvm", :id, "--memory", 4096]
        v.customize ["modifyvm", :id, "--name", machine[:hostname]]
        v.customize ["modifyvm", :id, "--cpus", 2]
      end
    end
  end

  id_rsa_key_pub = File.read(File.join(Dir.home, ".ssh", "id_rsa.pub"))

  config.vm.provision :shell,
        :inline => "echo 'appending SSH public key to ~vagrant/.ssh/authorized_keys' && echo '#{id_rsa_key_pub }' >> /home/vagrant/.ssh/authorized_keys && chmod 600 /home/vagrant/.ssh/authorized_keys"

  config.ssh.insert_key = false
end
```

To create the machine:

```
vagrant up --provider=hyperv
```

Now, we need to SSH into this machine and install Virtualbox.

But this would require Nested Virtualization. To allow this we are following [thi](https://superuser.com/questions/1330271/configuring-kvm-inside-ubuntu-hyper-v-client) tutorial.

First, we need to get the PowerShell [script made by Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/master/hyperv-tools/Nested/Enable-NestedVm.ps1):

```powershell
param([string]$vmName)
#
# Enable-NestedVm.ps1
#
# Checks VM for nesting comatability and configures if not properly setup.
#
# Author: Drew Cross

if([string]::IsNullOrEmpty($vmName)) {
    Write-Host "No VM name passed"
    Exit;
}

# Constants
$4GB = 4294967296

#
# Need to run elevated.  Do that here.
#

# Get the ID and security principal of the current user account
$myWindowsID = [System.Security.Principal.WindowsIdentity]::GetCurrent();
$myWindowsPrincipal = New-Object System.Security.Principal.WindowsPrincipal($myWindowsID);

# Get the security principal for the administrator role
$adminRole = [System.Security.Principal.WindowsBuiltInRole]::Administrator;

# Check to see if we are currently running as an administrator
if ($myWindowsPrincipal.IsInRole($adminRole)) {
    # We are running as an administrator, so change the title and background colour to indicate this
    $Host.UI.RawUI.WindowTitle = $myInvocation.MyCommand.Definition + "(Elevated)";

    } else {
    # We are not running as an administrator, so relaunch as administrator

    # Create a new process object that starts PowerShell
    $newProcess = New-Object System.Diagnostics.ProcessStartInfo "PowerShell";

    # Specify the current script path and name as a parameter with added scope and support for scripts with spaces in it's path
    $newProcess.Arguments = "& '" + $script:MyInvocation.MyCommand.Path + "'"

    # Indicate that the process should be elevated
    $newProcess.Verb = "runas";

    # Start the new process
    [System.Diagnostics.Process]::Start($newProcess) | Out-Null;

    # Exit from the current, unelevated, process
    Exit;
    }

#
# Get Vm Information
#

$vm = Get-VM -Name $vmName

$vmInfo = New-Object PSObject
    
# VM info
Add-Member -InputObject $vmInfo NoteProperty -Name "ExposeVirtualizationExtensions" -Value $false
Add-Member -InputObject $vmInfo NoteProperty -Name "DynamicMemoryEnabled" -Value $vm.DynamicMemoryEnabled
Add-Member -InputObject $vmInfo NoteProperty -Name "SnapshotEnabled" -Value $false
Add-Member -InputObject $vmInfo NoteProperty -Name "State" -Value $vm.State
Add-Member -InputObject $vmInfo NoteProperty -Name "MacAddressSpoofing" -Value ((Get-VmNetworkAdapter -VmName $vmName).MacAddressSpoofing)
Add-Member -InputObject $vmInfo NoteProperty -Name "MemorySize" -Value (Get-VMMemory -VmName $vmName).Startup


# is nested enabled on this VM?
$vmInfo.ExposeVirtualizationExtensions = (Get-VMProcessor -VM $vm).ExposeVirtualizationExtensions

Write-Host "This script will set the following for $vmName in order to enable nesting:"
    
$prompt = $false;

# Output text for proposed actions
if ($vmInfo.State -eq 'Saved') {
    Write-Host "\tSaved state will be removed"
    $prompt = $true
}
if ($vmInfo.State -ne 'Off' -or $vmInfo.State -eq 'Saved') {
    Write-Host "Vm State:" $vmInfo.State
    Write-Host "    $vmName will be turned off"
    $prompt = $true         
}
if ($vmInfo.ExposeVirtualizationExtensions -eq $false) {
    Write-Host "    Virtualization extensions will be enabled"
    $prompt = $true
}
if ($vmInfo.DynamicMemoryEnabled -eq $true) {
    Write-Host "    Dynamic memory will be disabled"
    $prompt = $true
}
if($vmInfo.MacAddressSpoofing -eq 'Off'){
    Write-Host "    Optionally enable mac address spoofing"
    $prompt = $true
}
if($vmInfo.MemorySize -lt $4GB) {
    Write-Host "    Optionally set vm memory to 4GB"
    $prompt = $true
}

if(-not $prompt) {
    Write-Host "    None, vm is already setup for nesting"
    Exit;
}

Write-Host "Input Y to accept or N to cancel:" -NoNewline

$char = Read-Host

while(-not ($char.StartsWith('Y') -or $char.StartsWith('N'))) {
    Write-Host "Invalid Input, Y or N" 
    $char = Read-Host
}


if($char.StartsWith('Y')) {
    if ($vmInfo.State -eq 'Saved') {
        Remove-VMSavedState -VMName $vmName
    }
    if ($vmInfo.State -ne 'Off' -or $vmInfo.State -eq 'Saved') {
        Stop-VM -VMName $vmName
    }
    if ($vmInfo.ExposeVirtualizationExtensions -eq $false) {
        Set-VMProcessor -VMName $vmName -ExposeVirtualizationExtensions $true
    }
    if ($vmInfo.DynamicMemoryEnabled -eq $true) {
        Set-VMMemory -VMName $vmName -DynamicMemoryEnabled $false
    }

    # Optionally turn on mac spoofing
    if($vmInfo.MacAddressSpoofing -eq 'Off') {
        Write-Host "Mac Address Spoofing isn't enabled (nested guests won't have network)." -ForegroundColor Yellow 
        Write-Host "Would you like to enable MAC address spoofing? (Y/N)" -NoNewline
        $input = Read-Host

        if($input -eq 'Y') {
            Set-VMNetworkAdapter -VMName $vmName -MacAddressSpoofing on
        }
        else {
            Write-Host "Not enabling Mac address spoofing."
        }

    }

    if($vmInfo.MemorySize -lt $4GB) {
        Write-Host "VM memory is set less than 4GB, without 4GB or more, you may not be able to start VMs." -ForegroundColor Yellow
        Write-Host "Would you like to set Vm memory to 4GB? (Y/N)" -NoNewline
        $input = Read-Host 

        if($input -eq 'Y') {
            Set-VMMemory -VMName $vmName -StartupBytes $4GB
        }
        else {
            Write-Host "Not setting Vm Memory to 4GB."
        }
    }
    Exit;
}

if($char.StartsWith('N')) {
    Write-Host "Exiting..."
    Exit;
}

Write-Host 'Invalid input'
```

Got to a directory and save this file as *enable-nested.ps1*.

Now, open Powershell in Administrator mode and change the ExceptionPolicy:

```powershell
Set-ExecutionPolicy -ExecutionPolicy Bypass
```

Now, we are able to run scripts:

```
.\enable-nested.ps1 [YOUR VM NAME INSIDE HYPERV]
```

Press Y multiple times.

Now, SSH into our machine. Open a CMD as Administrator and:

```
vagrant ssh wbsv1
```

Now, we need to install virtualbox.

There are a couple steps, I've used [this](https://www.itzgeek.com/how-tos/linux/centos-how-tos/install-virtualbox-4-3-on-centos-7-rhel-7.html) and [this](https://www.tecmint.com/install-virtualbox-on-redhat-centos-fedora/) tutorials.

But I'm gonna make things easier for you:

```
sudo su
yum update -y
yum install -y kernel-devel kernel-headers gcc make perl wget vim
wget https://www.virtualbox.org/download/oracle_vbox.asc
wget http://download.virtualbox.org/virtualbox/rpm/el/virtualbox.repo -O /etc/yum.repos.d/virtualbox.repo
yum install -y VirtualBox-6.0
uname -r
yum install kernel-devel-CURRENT_KERNEL
/usr/lib/virtualbox/vboxdrv.sh setup
systemctl status vboxdrv
systemctl start vboxdrv
systemctl status vboxdrv
```

As you can see, VirtualBox will return something like this:

```
● vboxdrv.service - VirtualBox Linux kernel module
   Loaded: loaded (/usr/lib/virtualbox/vboxdrv.sh; enabled; vendor preset: disabled)
   Active: active (exited) since Wed 2019-04-03 16:18:58 UTC; 10min ago
  Process: 63501 ExecStart=/usr/lib/virtualbox/vboxdrv.sh start (code=exited, status=0/SUCCESS)

Apr 03 16:18:58 wbsv1 systemd[1]: Starting VirtualBox Linux kernel module...
Apr 03 16:18:58 wbsv1 vboxdrv.sh[63501]: vboxdrv.sh: Starting VirtualBox services.
Apr 03 16:18:58 wbsv1 systemd[1]: Started VirtualBox Linux kernel module.
```

Now, we need to install:

1. Docker
2. Kubectl
3. Minikube

For Docker:

```
yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install docker-ce
systemctl enable docker.service
systemctl start docker.service
docker run hello-world
``` 

Para o Kubectl:

```
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

yum install -y kubectl
```

Para o Minikube:

```
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
chmod +x minikube
mv minikube /usr/bin
```

Agora, veremos se nosso cluster local está funcionando:

```
minikube start
```