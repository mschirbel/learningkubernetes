# ReplicaSets

Em um sistema de produção trabalharemos com deployments ou com ReplicaSets

Pods são efêmeros, muito comum de morrerem. Se um Pod consumir muita CPU, K8s mata o Pod.

Mas se fizermos um deploy direto(com *kubectl apply*) então somos responsáveis pela vida do Pod. Se um pod morrer, ele não vai voltar a vida. Para suprir isso, existem os ReplicaSets.

E é por isso que não fazemos o deploy de pods diretamente, fazer de replicasets, pois eles são duráveis e se auto regeneram.

Uma replicaset é uma nova camada para o Kubernetes, nela especificamos quantas replicas de um pod queremos que o K8s rode em todo o tempo. Ou seja, se esse Pod morrer, o K8s vai criar um novo.

## Group ReplicaSet

Na documentação vemos que o ReplicaSet não faz parte do grupo core de desenvolvimento da aplicação do K8s. Mas sim de um grupo chamado *apps*. Ou seja, quando formos inserir a apiVersion no código, devemos especificar o grupo a qual pertence.

Quando formos escrever o yaml do ReplicaSet, veremos que o Pod estará aninhado dentro do código do ReplicaSet dentro de uma tag chamada *template*, que é um template para nosso Pod.

nesse exemplo:

- especificamos o grupo
- colocamos labels
- colocamos um número para replicas
- temos um template que é identico ao nosso arquivo antigo de pods

Agora vamos escrever nosso ReplicaSet:

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: webapp
spec:
  replicas: 2
  template: # template for the pods
    metadata:
      labels:
        app: webapp
    spec:
      containers:
      - name: webapp
        image: richardchesterwood/k8s-fleetman-webapp-angular:release0-5
```

Aqui vemos que toda a definição do Pod foi parar dentro da tag *template*.

Uma diferença é que não precisamos mais do nome dos Pods. Podemos retirá-lo. Mas devemos manter os labels, pois eles vão linkar com o selector dos services.

Para aplicar:

```
kubectl apply -f replicasets/pods.yaml
```

Verá que temos um error, ele pede um parâmetro ou tag de selector. Isso significa que precisamos de um block de selector dentro do nosso replicaset graças a versão de nossa API.

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: webapp
spec:
  selector:
    matchLabels:
      app: webapp
  replicas: 2
  template: # template for the pods
    metadata:
      labels:
        app: webapp
    spec:
      containers:
      - name: webapp
        image: richardchesterwood/k8s-fleetman-webapp-angular:release0-5
```

Agora podemos reaplicar o arquivo:

```
kubectl delete pods --all
kubectl get all
kubectl apply -f replicasets/pods.yaml
kubectl get all
```

Ao vermos o replicaset online, percebemos que existem 3 colunas:

- Desired => quantos containers deverão ser alocados
- Current => quantos containers foram criados
- Ready   => quantos containers respondem ao ping

Perceba que o replicaset também criou um Pod com um madeup name.

```
kubectl describe replicaset webapp
```

Se formos ao browser, veremos que a aplicação não responde, isso acontece porque removemos a tag de released do nosso Pod.

Para corrigir isso podemos remover esse selector do nosso service.

```
apiVersion: v1
kind: Service
metadata:
  name: fleetman-webapp

spec:
  # This defines which pods are going to be represented by this Service
  # The service becomes a network endpoint for either other services
  # or maybe external users to connect to (eg browser)
  selector:
    app: webapp

  ports:
    - name: http
      port: 80
      nodePort: 30080

  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: fleetman-queue

spec:
  # This defines which pods are going to be represented by this Service
  # The service becomes a network endpoint for either other services
  # or maybe external users to connect to (eg browser)
  selector:
    app: queue

  ports:
    - name: http
      port: 8161
      nodePort: 30010

  type: NodePort
```

Agora reaplique o service

```
kubectl apply -f replicasets/services.yaml
kubectl get all
```

Se tivéssemos 2 ou mais replicas mesmo se um Pod fosse deletado, nossa aplicação ainda estaria de pé.