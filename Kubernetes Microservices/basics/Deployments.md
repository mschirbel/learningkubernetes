# Deployment

Deployments são uma forma mais sofisticada de ReplicaSets. É quase a mesma coisa, porém, com deployments temos rolling updates com 0 downtime. 

Com replicasets podemos fazer isso manualmente, mas com deployments é algo mais elegante.

Veja o exemplo da documentação:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```

Praticamente podemos reutilizar o que fizemos até agora.

Se encerrarmos um replicaset, nossos pods também serão eliminados.

Para o nosso arquivo:

```

apiVersion: apps/v1
kind: Deployment
metadata:
  name: webapp
spec:
  # minReadySeconds: 30
  selector:
    matchLabels:
      app: webapp
  replicas: 2
  template: # template for the pods
    metadata:
      labels:
        app: webapp
    spec:
      containers:
      - name: webapp
        image: richardchesterwood/k8s-fleetman-webapp-angular:release0-5
```

Agora aplicamos nosso arquivo

```
kubectl apply -f deployments/pods.yaml
kubectl get all
```

Um deployment vai criar um replicaset e por sua vez, um pod. Perceba que o RS vai ter um nome aletório, bem como o pod, herdando o nome do RS e mais uma parte aleatória.

Quando fizermos uma alteração no nosso arquivo o K8s vai criar um novo ReplicaSet. E quando os Pods do novo RS estiverem respondendo as requisições, o antigo RS terá o parâmetro de replicas alterado para 0. Caso seja necessário um rollback. Assim é feito um rolling update e garantido o rollback.

## Managing Rollouts

Para saber o status do deployment:

```
kubectl rollout status deployment [deployment name]
kubectl rollout status deployment webapp
```

Isso dirá o status do deployment realizado.

Podemos ver o history de deployments

```
kubectl rollout history deployment webapp
``` 

Para fazer um rollback de um deployment:

```
kubectl rollout undo deployment webapp
```

Para fazer um rollback específico:

```
kubectl rollout undo deployment webapp --to-revision=[id da revision]
```

Uma revision pode ser encontrada no history. K8s vai lembrar as últimas 10 mudanças.
Mas isso tem um problema, ao fazer um rollback, os arquivo yaml ainda indicarão a versão nova, e isso não é o que queremos, pois os arquivos devem descrever o atual estado do sistema/cluster.

**Isso só deve ser usado como emergência. Nunca como boas práticas.**