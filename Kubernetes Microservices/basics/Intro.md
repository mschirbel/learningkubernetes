# Introduction

Não é necessário nenhum conhecimento prévio. Talvez um pouco de docker, mas teremos uma seção para isso.

No fim, moveremos nosso cluster de local para a AWS.

## Kubernetes

Kubernetes é um orquestrador de container. Enquanto Docker é um gerenciador de containers, o Kubernetes está num nível acima, pois podemos controlar muito mais do que somente os containers.

Kubernetes foi desenvolvido para administrar muitos containers, como numa arquitetura de microservices. E para previnir a queda de containers sem uma política de failover.

K8s funciona com *manifests*. Estes são arquivos declarativos para administrarmos nossos containers, storages, networks.

Isso deixa o sistema robusto. E possível de fazer um healthcheck.