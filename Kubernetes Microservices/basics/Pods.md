# Pods

Pods são a parte mais simples do Kubernetes. Eles são compostos de um ou mais containers de mesma imagem.

Pods podem ser um embrulho para um container. Um ou mais containers.

Na maioria dos casos, pods estarão 1-1 para os containers.

Nesse curso, usaremos a image: k8s-fleetman-webapp-angular

As tags são:

1. release0
2. release0-5
3. release1
4. release2

## Writing

Pods são escritos em yaml.

Um básico é o seguinte:

```
apiVersion: v1
kind: Pod
metadata:
    name: pod
spec:
    containers:
    - name: ubuntu
      image: ubuntu:alpine
      command: ["echo"]
      args: ["Hello World"]
```

Agora vamos criar o nosso pod.

Agora basta criar um arquivo com esse conteudo:

```
apiVersion: v1
kind: Pod
metadata:
  name: webapp
spec:
  containers:
  - name: webapp
    image: richardchesterwood/k8s-fleetman-webapp-angular:release0
```

Para encontrarmos algo em nosso cluster de K8s:

```
kubectl get all
```

Veremos um serviço do kubernetes por default que é cluster de nossos containers

Para fazer um deploy de um Pod para esse Cluster

```
kubectl apply -f first-pod.yaml
```

-f é para especificar um nome de arquiivo.

Se rodar um *kubectl get all* verá que o pod está criado.

Para vermos a aplicação rodando do pod temos que ter uma porta e um IP fora do Cluster. Por default, o Pod não é visível fora do Cluster K8s.

Para visitarmos o Cluster no browser, precisamos do IP do minikube:

```
minikube ip
```

Se você colocar esse IP no browser, não verá nada. Isso só será possível quando aprendermos sobre serviços.

### Mais infos sobre o Pod

```
kubectl describe pod [name of the pod]
```

Uma das coisas mais interessantes é a seção de Events, onde podemos ver uma log sobre a criação do container dentro do Pod.

### Entrarmos no Container dentro do Pod

```
kubectl exec [name of the pod] [command]
kubectl -it exec webapp /bin/bash
```

Para visualizarmos a aplicação dentro do container podemos rodar um comando dentro do container.

```
kubectl -it exec webapp /bin/bash
wget http://localhost:80
cat index.html
```