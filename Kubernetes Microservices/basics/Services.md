# Services

No último exemplo, não conseguimos acessar nossa aplicação via browswer.

Isso acontece porque os Pods não são visíveis fora do Cluster.
Pods são desenhados para serem efêmeros, por isso não podemos confiar neles para armazenar nosso site.

Para suprir isso, temos os Serviços, que é um objeto durável com um IP e Pods continuamente durante a exitência do Service.

Mas como um service sabe que um Pod pode suprir uma request do browser?
Por meio de um key-value pair. Ou seja, quando um serviço identificar que uma tag entre ele e um pod é a mesma, ele serve a request com aquele Pod.

No Pod, teremos um Label
No Service, teremos um Selector.

## Subir um service

```
minikube start
```

Agora veja qual o estado do Cluster, pois o Minikube lembra o último estado válido:

```
kubectl get all
```

Agora, vamos criar um serviço, para isso, precisaremos das informações do nosso Pod:

```
kubectl describe webapp
```

Veja o exemplo da documentação:

```yaml
kind: Service
apiVersion: v1
metadata:
    name: service-example
spec:
    ports:
        - name: http
          port: 80
          targetPort: 80
    selector:
        app: nginx
    type: LoadBalancer
```

Veja a seção onde definimos nosso seletor, nessa tag de APP devemos colocar a mesma no nosso pod. Quando o service procurar por um Pod, vai encontrar nosso webapp.

Em port devemos colocar a porta do container. Em targetPort a porta que acessaremos pelo browser.

## Escrevendo o Pod

```yaml

apiVersion: v1
kind: Service
metadata:
  name: fleetman-webapp

spec:
  # This defines which pods are going to be represented by this Service
  # The service becomes a network endpoint for either other services
  # or maybe external users to connect to (eg browser)
  selector:
    app: webapp
    release: "0-5"

  ports:
    - name: http
      port: 80
      nodePort: 30080

  type: NodePort
```

LoadBalancer é geralmente usado para cloud. Outras opções são
- ClusterIP => isso significa que será acessível somente do Cluster, não do Browser
- NodePort => isso significa que será acessível de fora do cluster

Isso é importante, pois o Service é um objeto estável. Quando queremos que algo seja acessível pelo browser,devemos usar NodePort. E quando escolhemos o tipo como NodePort, temos a opção de colocar uma nodePort.

Mas a nodePort só pode utilizar portas acima de 30000.

Para fazer o deploy:

```
kubectl apply -f webapp-service.yaml
kubectl get all
```

Veja que o externalIP está como none - isso é normal, só funciona com LoadBalancers, mas veja que as portas estão corretas.

Mas falta um passo crucial, pois se acessarmos o IP do minikube com a porta do nosso service, nada será respondido.

O service funciona via o selector, entretanto, nosso Pod não está com o Label correto. Para isso, devemos criar um label no yaml de nosso pod:

```
apiVersion: v1
kind: Pod
metadata:
  name: webapp
  labels:
    name: webapp
spec:
  containers:
  - name: webapp
    image: richardchesterwood/k8s-fleetman-webapp-angular:release0
```

Veja que agora o Label corresponde ao Selector

Para modificar o que já está criado:

```
kubectl apply -f first-webapp-service.yaml
kubectl apply -f first-pod.yaml
```

Veja que agora não apareceu configured, mas sim *configured*.

---

## Deploy with zero downtime

Para fazer um deploy de uma nova imagem, poderíamos simplesmente reescrever a linha do pod onde fazemos o pull da image. Mas isso ocorreria num downtime, pois o pod seria destruido e teríamos que baixar a nova image.

Para fazer um deploy sem downtime, usamos os Labels

Vamos adicionar um label de *release* ao nosso pod. Ou seja, o service vai procurar os Pods que tenham o label de webapp e também tiverem o label com o release atual.

Ao trocar o label no Selector, o service vai procurar pelo novo pod que criaremos.

Para criar um segundo Pod dentro do mesmo arquivo do segundo faça do seguinte jeito:

```

apiVersion: v1
kind: Pod
metadata:
  name: webapp
  labels:
    name: webapp
    release: "0"
spec:
  containers:
  - name: webapp
    image: richardchesterwood/k8s-fleetman-webapp-angular:release0

---

apiVersion: v1
kind: Pod
metadata:
  name: webapp-release-0-5
  labels:
    name: webapp
    release: "0-5"
spec:
  containers:
  - name: webapp
    image: richardchesterwood/k8s-fleetman-webapp-angular:release0

```

Para o Service:

```
apiVersion: v1
kind: Service
metadata:
  name: fleetman-webapp

spec:
  # This defines which pods are going to be represented by this Service
  # The service becomes a network endpoint for either other services
  # or maybe external users to connect to (eg browser)
  selector:
    app: webapp
    release: "0-5"

  ports:
    - name: http
      port: 80
      nodePort: 30080

  type: NodePort
```

```
kubectl apply -f first-pod.yaml
kubectl apply -f first-webapp-service.yaml
kubectl get all
kubectl describe service fleetman-webapp
kubectl get pods --show-labels
```