# Introduction

Esse curso precisa de conhecimentos prévios em K8s.

Vamos ver:

1. Instalando K8s on-prem ou cloud usando kubeadm
2. K8s Operators
3. Cert management
4. LDAP auth
5. Envoy, Istio
6. Calico networking
7. Secret with vault
8. Openshift origin

## Trail Map

O primeiro passo, é entender containers. Existem algumas ferramentas para fazer isso, mas a mais famosa é o Docker.
Depois, precisamos atribuir uma esteira de CI/CD. 
Isso deve ser orquestrado de forma automatizada, e para isso usamos o K8s.

Caso queira ver o Trail Map completo da Cloud Nativa:

![](https://d30j409nn93ixf.cloudfront.net/2018/12/13235420/CNCF_TrailMap_latest.png)