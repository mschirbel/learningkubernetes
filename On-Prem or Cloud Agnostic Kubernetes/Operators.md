# Operators

É um metodo de fazer packages, deploys e de controlar uma apicação no Kubernetes.
É uma camada de abstração para remover essa parte de conhecimento técnico do computador e trazer mais próximos os conhecimentos da nuvem.

Depois de configurado, pode ser administrado via CRD(Custom Resource Definition).

Serve para fazer o deploy de *stateful* services. Para saber mais sobre *stateful* e *stateless* services leia [aqui](https://www.xenonstack.com/insights/what-are-stateful-and-stateless-applications/).

CTDs são extensões da API do K8s. Ou seja, podemos deletar, adicionar, modificar os objetos.

## Example

1. etcd
2. Rook
3. Prometheus
4. Vault
5. outros

## etcd

É um serviço que guarda *key-value* pairs.

Um cluster pode ser criado da [seguinte maneira](https://github.com/coreos/etcd-operator/blob/master/example/example-etcd-cluster.yaml):

```yaml
apiVersion: "etcd.database.coreos.com/v1beta2"
kind: "EtcdCluster"
metadata:
  name: "example-etcd-cluster"
  ## Adding this annotation make this cluster managed by clusterwide operators
  ## namespaced operators ignore it
  # annotations:
  #   etcd.database.coreos.com/scope: clusterwide
spec:
  size: 3
  version: "3.2.13"
```

Em apiVersion veja que estamos usando uma diferentes da *default*, isso que quis dizer com estender a API do K8s.

Para trocar o tamanho do cluster, ou fazer o update de versão, basta alterar no arquivo yaml e rodar um *kubectl apply*.

### Other examples

Existem outros tipos de Operators, como por exemplo:

**PostgreSQL** que foi criado por [Zalando](https://github.com/zalando/postgres-operator).

Ou até mesmo o MySQL criado pela [Oracle](https://github.com/oracle/mysql-operator).

## Tools to build new operators

1. The Operator SDK
2. Operator Lifecycle Manager
3. Operator Metering

O SDK serve para criar o Operator sem ter que saber a API do k8S.
O Lifecycle serve para fazer updates do seu Operator
E o Metering serve para fazer reports

## Rook

Rook é um operador para sistemas de storage distribuídos.

Podemos ver o código [aquii](https://rook.io/docs/rook/master/).

Podemos usar sistemas de storage nos clusters de K8s independentemente do vendor de cloud. Ou até  mesmo on-prem.
Geralmente é comum colocar um storage num Pod para fazer a persistência de dados.
Claro que é muito mais fácil fazer isso na nuvem, como AWS com o EBS, por exemplo.

Rook visa facilitar isso pra você, independente do provider ou mesmo se for on-prem.

Atualmente o Rook usa o Ceph como base do storage. É a solução mais estável até agora.

![](https://github.com/rook/rook/blob/master/Documentation/media/rook-architecture.png)
*Fonte: https://github.com/rook/rook/blob/master/Documentation/media/rook-architecture.png*

Kubectl é a cli que usamos para nos comunicar com a API do K8s. Toda a configuração é guardada num DB distribuído, chamado de *etcd*.
E assim teremos o operador Rook, que usa um cluster de Rook. Para a comunicação do Cluster com o sistema de storage(Ceph, por exemplo) usamos o Agent.
Tudo isso é configurado via kubelet, ou seja, dentro do próprio node.

## Ceph

É um software open-source para fazer storage, seja em objetos, arquivos ou em blocos de dados.

Replica os dados para que, mesmo um node caindo, ainda tenhamos acesso as informações.

É *self-healing* e altamente escalável.

Tem 3 tipos de storages:

1. File Storage: para acessar NFS ou NAS, EFS(aws) para guardar arquivos e diretórios.
2. Block Storage: para acessar SAN, EBS(aws) para guardar usando um filesystem, como um HD.
3. Object Storage: qualquer tipo de dado como um objeto, usando keys para guardar metadados. Como o S3(aws).

### Components

1. Ceph Monitor - mantém um mapa do cluster para comunicação de componentes e de daemons e clients.
2. Ceph Manager Daemon - guarda o estado do cluster
3. Ceph OSD - Daemon para Object Storage - faz a replicação, recuperação e rebalanceamento de dados
4. Ceph Metadata Server - guarda os metadados para o tipo de storage usado
5. RADOS - usado só para o Object Storage, provê *self-healing*, e auto distribuição para os nodes.
6. RBD - Rados Block Device - para o Block Storage. Guarda o block como se fosse um objeto.

Ceph usa o **CRUSH algorithm (Controlled Replication Under Scalable Hashing)** para fazer o scaling dos dados entre múltiplos nodes e múltiplos daemons.

Então, os dados podem vir de *File Storage*, ou de *Block Storage* ou de *Object Storage*, mas todos passam pelo **RADOS** e são armazenados como objetos pelo Ceph dentro do OSD(Object Storage Device), em um node diferente.

## Ceph with Rook

![](https://github.com/rook/rook/blob/master/Documentation/media/kubernetes.png)

Em azul, temos a arquitetura do Rook. Com os Agents em cada Node.
Em vermelho, temos a arquitetura do Ceph.

OSD's é onde os dados são guardados, podendo ser em múltiplos Nodes.
Em cima, temos os daemons, que servem para guardar o estado do cluster.

## Demo

Dentro de *resources/rook/* temos os arquivos necessários.

Como por exemplo o operator do Rook para criar o Rook:

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: rook-system
---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: rook-operator
rules:
- apiGroups:
  - ""
  resources:
  - namespaces
  - serviceaccounts
  - secrets
  - pods
  - services
  - nodes
  - nodes/proxy
  - configmaps
  - events
  - persistentvolumes
  - persistentvolumeclaims
  verbs:
  - get
  - list
  - watch
  - patch
  - create
  - update
  - delete
- apiGroups:
  - extensions
  resources:
  - thirdpartyresources
  - deployments
  - daemonsets
  - replicasets
  verbs:
  - get
  - list
  - watch
  - create
  - update
  - delete
- apiGroups:
  - apiextensions.k8s.io
  resources:
  - customresourcedefinitions
  verbs:
  - get
  - list
  - watch
  - create
  - delete
- apiGroups:
  - rbac.authorization.k8s.io
  resources:
  - clusterroles
  - clusterrolebindings
  - roles
  - rolebindings
  verbs:
  - get
  - list
  - watch
  - create
  - update
  - delete
- apiGroups:
  - storage.k8s.io
  resources:
  - storageclasses
  verbs:
  - get
  - list
  - watch
  - delete
- apiGroups:
  - rook.io
  resources:
  - "*"
  verbs:
  - "*"
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: rook-operator
  namespace: rook-system
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: rook-operator
  namespace: rook-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: rook-operator
subjects:
- kind: ServiceAccount
  name: rook-operator
  namespace: rook-system
---
apiVersion: apps/v1beta1
kind: Deployment
metadata:
  name: rook-operator
  namespace: rook-system
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: rook-operator
    spec:
      serviceAccountName: rook-operator
      containers:
      - name: rook-operator
        image: rook/rook:v0.7.1
        args: ["operator"]
        env:

        - name: ROOK_MON_HEALTHCHECK_INTERVAL
          value: "45s"

        - name: ROOK_MON_OUT_TIMEOUT
          value: "300s"
        - name: NODE_NAME
          valueFrom:
            fieldRef:
              fieldPath: spec.nodeName
        - name: POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: POD_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace

```

Precisaremos uma Role, com todos os acessos necessários, um serviço para export o Rook e um deployment para criar o objeto.

Agora, crie o deployment:

```
kubectl create -f rook-operator.yaml
```

Veja se os pods estão criados:

```
kubectl get pods -n rook-system
```

E agora, vamos adicionar o cluster. Podemos ver o arquivo *rook-cluster.yaml*:

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: rook
---
apiVersion: rook.io/v1alpha1
kind: Cluster
metadata:
  name: rook
  namespace: rook
spec:

  backend: ceph

  dataDirHostPath: /var/lib/rook

  hostNetwork: false

  monCount: 3 #monitoring daemons

  resources:

  storage: 
    useAllNodes: true
    useAllDevices: false
    deviceFilter:
    metadataDevice:
    location:
    storeConfig:
      storeType: bluestore
      databaseSizeMB: 1024 
      journalSizeMB: 1024
```

Todo o nossos dados, ficarão guarados em */var/lib/rook/*, assim como setado no Cluster.

Inicie o cluster:

```
kubectl create -f rook-cluster.yaml
```

Veja se foi criado:

```
kubectl get pods -n rook-system
```

E agora, temos que fazer uma classe para nosso storage:

```yaml
apiVersion: rook.io/v1alpha1
kind: Pool
metadata:
  name: replicapool
  namespace: rook
spec:
  replicated:
    size: 2

---
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
   name: rook-block
provisioner: rook.io/block
parameters:
  pool: replicapool

  clusterName: rook
```

Isso poderá replicar nossos dados mesmo se um Pod morrer.

Crie a classe

```
kubectl create -f rook-storageclass.yaml
```

Veja se foi criado:

```
kubectl get pods -n rook-system
```

E também podemos ver o estado do cluster usando o *rook-tools*:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: rook-tools
  namespace: rook
spec:
  dnsPolicy: ClusterFirstWithHostNet
  containers:
  - name: rook-tools
    image: rook/toolbox:v0.7.1
    imagePullPolicy: IfNotPresent
    env:
      - name: ROOK_ADMIN_SECRET
        valueFrom:
          secretKeyRef:
            name: rook-ceph-mon
            key: admin-secret
    securityContext:
      privileged: true
    volumeMounts:
      - mountPath: /dev
        name: dev
      - mountPath: /sys/bus
        name: sysbus
      - mountPath: /lib/modules
        name: libmodules
      - name: mon-endpoint-volume
        mountPath: /etc/rook
  hostNetwork: false
  volumes:
    - name: dev
      hostPath:
        path: /dev
    - name: sysbus
      hostPath:
        path: /sys/bus
    - name: libmodules
      hostPath:
        path: /lib/modules
    - name: mon-endpoint-volume
      configMap:
        name: rook-ceph-mon-endpoints
        items:
        - key: data
          path: mon-endpoints
```

Isso nos permite fazer diagnósticos do cluster.

```
kubectl create -f rook-tools.yaml
```

Veja se foi criado:

```
kubectl get pods -n rook-system
```

Agora, veja os containers dos nossos tools:

```
kubectl exec -it rook-tools -n rook -- bash
```

Para ver status do Rook:

```
rookctl status
```

## MySQL examplo

Crie o nosso MySQL de exemplo, que está em *resources/rook/mysql-demo.yaml*:

```
kubectl create -f mysql-demo.yaml
```

```
kubectl get pods
kubectl get pv
```

Agora podemos executar comandos dentro do Pod de MySQL

```
kubectl exec -it <mysql pod name> -- bash
```

Podemos ver os arquivos para guardar os dados
```
ls /var/lib/mysql
```

Entre no Banco e execute alguns comandos

```
mysql -uroot -pchangeme
create databasedemo;
use databasedemo;
create table hello(id int);
insert into hello values(1);
insert into hello values(2);
insert into hello values(3);
select * from hello;
```

Agora, vamos destruir nosso node:

``` 
kubectl describe pod/<mysql pod>
kubectl cordon <node que o pod mysql está rodando>
```

Veja que o node foi destruído:

``` 
kubectl get nodes
```

Delete o pod:

```
kubect delete pod <mysql pod>
kubectl get pod
kubectl describe pod/<mysql pod>
```

Entre no Pod e veja se os dados continuam
```
kubectl exec -it <mysql pod name> -- bash
use databasedemo;
select * from hello;
```

Mesmo se você entrar no Node que está com esse Pod e fizer um shutdown:

```
shutdown now
```

Os dados continuaram persistidos:

```
use databasedemo;
select * from hello;
```

Se você conferir as *rook-tools* verá que temos um warning:

```
kubectl exec -it -n rook rook-tools -- bash
rookctl status
```

## Rook with Object Storage

Primero, deixe o Node escalável novamente:

```
kubectl uncordon <node que estava o pod de mysql>
```

Agora, veja que no arquivo *resources/rook/rook-storageclass-objectstore.yaml* temos um outro time de armazenamento:

```yaml
apiVersion: rook.io/v1alpha1
kind: ObjectStore
metadata:
  name: my-store
  namespace: rook
spec:
  metadataPool:
    replicated:
      size: 3
  dataPool:
    erasureCoded:
      dataChunks: 2
      codingChunks: 1
  gateway:
    type: s3
    sslCertificateRef:
    port: 80
    securePort:
    instances: 1
    allNodes: false
```

Isso é bom, pois podemos usar o S3 da AWS.

Veja os pods e os services:

```
kubectl get pods -n rook
kubectl get svc -n rook
```

O serviço é o S3 Gateway para conexão com a AWS. Podemos usar até dentro da aplicação.

Veja também a monitoração do rook-tools:

```
kubectl exec -it -n rook rook-tools -- bash
rookctl status
```

E crie um usuário:

```
radosgw-admin user create --uid rook-user --display-name "A rook rgw User" --rgw-realm=my-store --rgw-zonegroup=my-store
```

*radosgw-admin* é o modo como chamamos aquele serviço para se conectar com a AWS.

E configure as variáveis de ambiente necessárias:

```
export AWS_HOST=
export AWS_ENDPOINT=
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
```

Host e IP estarão listados em:

```
kubectl get svc -n rook
```
Antes de entrar no Pod!

E ACCESSKEY e SECRETKEY serão mostradas quando criarmos o usuário.

Temos a linha de comando para fazer os buckets, é o *s3cmd*.

Para criar um bucket:

```
s3cmd mb --no-ssl --host=${AWS_HOST}  --host-bucket = s3://demobucket
```

Para adicionar algo no bucket:

```
echo 'hello' > test.txt
s3cmd put test.txt --no-ssl --host=${AWS_HOST}  --host-bucket = s3://demobucket
```

Assim adicionamos o arquivo test.txt no bucket criado.

Para ver o conteúdo do bucket:

```
s3cmd ls --no-ssl --host=${AWS_HOST}  --host-bucket = s3://demobucket
```

## Rook with File Storage

Para ver a Storage class de File, veja o arquivo em *resources/rook/rook-storageclass-fs.yaml*

```yaml
apiVersion: rook.io/v1alpha1
kind: Filesystem
metadata:
  name: myfs
  namespace: rook
spec:
  metadataPool:
    replicated:
      size: 3
  dataPools:
    - erasureCoded:
       dataChunks: 2
       codingChunks: 1
  metadataServer:
    activeCount: 1
    activeStandby: true
```

Crie essa classe:

```
kubectl create -f rook-storageclass-fs.yaml
```

Não é bem uma storage class, é mais um file system.

E para demo, temos um Pod com ubuntu, que tem um file system montado em */data*.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu
spec:
  containers:
  - image: ubuntu:latest
    name: ubuntu
    command: [ "/bin/bash", "-c", "--" ]
    args: [ "while true; do sleep 300; done;" ]
    volumeMounts:
    - name: fs-store
      mountPath: /data
  volumes:
  - name: fs-store
    flexVolume:
      driver: rook.io/rook
      fsType: ceph
      options:
        fsName: myfs 
        clusterNamespace: rook
        clusterName: rook
```

```
kubectl create -f fs-demo.yaml
```

```
kubectl get pods -n rook
```

Veja que temos agora Pods com o nome *myfs*. E vamos ver como está nosso pod:

```
kubectl exec -it ubuntu --bash
mount |grep data
```

Veja que temos 3 IPs em /data. Ou seja, os 3 nodes estão reconhecendo o nosso diretório.

Se criarmos um arquivo em */data*, teremos isso replicado para os 3 nodes.

## Conclusion

Use sempre Object Storage, se possível. É mais robusto e escala melhor.