# Manage TLS Certificates

Para uma conexão segura(https) é necessário o uso de certificados.

Esses certificados podem ser comprados ou assinados por clouds públicas.
Na AWS temos o Certificate Manager.

E é sempre difícil/demorado para comprar e instalar. Pra isso temos o *cert-manager*, um componente que podemos instalar no K8s para controlar isso.

Ele usa o **letsencrypt** é uma Autoridade Certificadora. Mas precisamos provar que somos o donos do domínio. Isso é reconhecido pela maioria dos browsers.

Mas esses certificados precisam ser renovados, também. Mas o *cert-manager* cuida de renovar isso também.

![](https://docs.cert-manager.io/en/latest/_images/high-level-overview.png)

## Demo

Vamos usar o [Helm](https://github.com/kubernetes/helm/releases). Para instalar:

```
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash
```

Depois inicie:

```
kubectl create -f rbac-config.yml
helm init --service-account tiller
```

Helm vai instalar o tiller para nós, para isso, devemos criar uma Service Account para ele. Ela está descrita no arquivo *resources/helm/rbac-config.yaml*.

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: tiller
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: tiller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: tiller
    namespace: kube-system
```

Uma service account é um objeto que dá uma identidade para um processo dentro de um Pod.

Por exemplo, quando a gente a API do K8s, estamos nos autenticando como Admins. Mas quando um processo, dentro de um Pod, quiser usar essa API, ele precisa de uma Service Account. Para ler mais clique [aqui](https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/).

A Service Account acima, permite que o Helm instale qualquer coisa em qualquer namespace.

Dentro do diretório *resources/cert-manager/* temos os arquivos para instlá-lo. Mas precisamos antes acessar o nosso Cluster da porta 80 e da porta 443. Isso é um Ingress para o nosso Cluster.

Para criar um Ingress Controller:

```
helm install --name my-ingress stable/nginx-ingress \
  --set controller.kind=DaemonSet \
  --set controller.service.type=NodePort \
  --set controller.hostNetwork=true
```

E agora podemos criar uma aplicação de exemplo:

```
kubectl create -f myapp.yml
kubectl create -f myapp-ingress.yml
```

Agora só temos que fazer o nosso DNS apontar para o IP de algum dos Nodes. Podemos ver que o nosso DNS está no arquivo *myapp-ingress.yml*.

Podemos então instalar o Cert Manager:

```
helm install \
    --name cert-manager \
    --namespace kube-system \
    stable/cert-manager
```

E você verá uma mensagem dizendo que é necessário um ClusterIssuer. Temos isso no arquivo *resources/cert-manager/issuer-staging.yml*:

```yaml
apiVersion: certmanager.k8s.io/v1alpha1
kind: Issuer
metadata:
  name: myapp-letsncrypt-staging
  namespace: default
spec:
  acme:
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    email: <seu email>
    privateKeySecretRef:
      name: myapp-letsncrypt-staging
    http01: {}
```

```
kubectl create -f issuer-staging.yml
kubectl create -f issuer-prod.yml
```

E temos também o de prod:

```yaml
apiVersion: certmanager.k8s.io/v1alpha1
kind: Issuer
metadata:
  name: myapp-letsncrypt-prod
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    email: <seu email>
    privateKeySecretRef:
      name: myapp-letsncrypt-prod
    http01: {}
```

E agora temos que criar o objeto do nosso Issuer, o certificado. Temos um para staging e outro para prod:

```yaml
apiVersion: certmanager.k8s.io/v1alpha1
kind: Certificate
metadata:
  name: myapp
  namespace: default
spec:
  secretName: myapp-tls-staging
  issuerRef:
    name: myapp-letsncrypt-staging
  commonName: dominio
  acme:
    config:
    - http01:
        ingress: myapp
      domains:
      - homol.seudominio.com.br
```

```yaml
apiVersion: certmanager.k8s.io/v1alpha1
kind: Certificate
metadata:
  name: myapp
  namespace: default
spec:
  secretName: myapp-tls-prod
  issuerRef:
    name: myapp-letsncrypt-prod
  commonName: dominio
  acme:
    config:
    - http01:
        ingress: myapp
      domains:
      - prod.seudominio.com.br
```

**Apenas veja que o SecretName do Certificado deve ser correspondente com o nome do Issuer.**

```
kubectl create -f certificate-staging.yml
kubectl create -f certificate-prod.yml
kubectl describe certificates <nome da app>
kubectl describe ingress
```

Para fazer o certificado funcionar, no arquivo *resources/cert-manager/myapp-ingress.yml* edite a parte do TLS:

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
  name: myapp
  namespace: default
spec:
  tls:
  - secretName: myapp-tls-staging
    hosts:
    - myapp.newtech.academy
  rules:
    - host: myapp.newtech.academy
      http:
        paths:
          - backend:
              serviceName: myapp
              servicePort: 3000
            path: /

```

```
kubectl apply -f myapp-ingress.yml
```

Para ter um reconhecido, temos que usar o de produção:

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
  name: myapp
  namespace: default
spec:
  tls:
  - secretName: myapp-tls-prod
    hosts:
    - myapp.newtech.academy
  rules:
    - host: myapp.newtech.academy
      http:
        paths:
          - backend:
              serviceName: myapp
              servicePort: 3000
            path: /

```

Agora basta verificar o DNS no browser.