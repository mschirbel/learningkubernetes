# Istio & Envoy

Quando quebramos um sistema monolítico em vários microserviços, temos diversas partes que devem comunicar e trafegar informações entre si.

Essa comunicação deve ser rápida, confiável e flexível.
Para isso, usamos uma *service mesh*. Que é uma camada da infraestrutura para tratar dessa comunicação entre serviços.

Geralmente é feita usando *proxies*. Assim como o **envoy**.

## Envoy

É um proxy de alta performance, feito para a cloud e escrito em C++. Possui alta interação com Nginx, HAProxy e load balancer de diversas nuvens.

Features:

1. small memory footprint
2. transparente de http/1.1 para http/2, ou seja, funciona na maioria dos browsers
3. automatic retry
4. rate limit
5. request shadowing
6. zone balancing
7. a configuração pode ser feita via API

Ou seja, as features são bem parecidas com usadas em cloud. Envoy é bem leve e bem rápido. Tem menos features do que outros concorrentes, mas é bem rápido.

## Istio

É uma plataforma para conectar, controlar e deixar microserviços seguros.
Pode controlar o tráfego entre eles, deixando o sistema mais robusto e confiável. Ele usa autenticação e políticas de acesso entre os serviços.

![](https://istio.io/docs/concepts/what-is-istio/arch.svg)

O pilot envia as configurações para os proxies(envoy), e usando essa configuração eles podem fazer as comunicações entre os serviços.

Ou seja, um serviçoA fala para o envoyA o que precisa falar com o serviçoB. O envoyA fala isso para o envoyB e o envoyB para o serviçoB.

Essa comunicação é feita via HTTP/1.1 ou HTTP/2 ou gRPC ou TCP(com ou sem TLS).

## Components

1. Istio
2. Envoy como proxy do Istio. Cada pod vai ter um envoy
3. Mixer, responsável pelo acesso e policies de acesso
4. Pilot, controle de tráfego e resiliência.
5. Istio Auth, autenticação entre serviços e com usuários.

## Demo

Temos os arquivos em *resources/istio/*.

Vamos fazer o download da versão 0.7.1:

```
wget https://github.com/istio/istio/releases/download/0.7.1/istio-0.7.1-linux.tar.gz
tar -xzvf istio-0.7.1-linux.tar.gz
cd istio-0.7.1
echo 'export PATH="$PATH:/home/ubuntu/istio-0.7.1/bin"' >> ~/.profile
```

Agora podemos usar o binário do istio:

```
istioctl version
```

E agora, vamos fazer o deploy do istio:

```
kubectl apply -f install/kubernetes/istio.yaml
```
Se receber um erro falando que não encontrou algum kind, rode o comando novamente.

E para fazer com TLS:

```
kubectl apply -f install/kubernetes/istio-auth.yaml
```

Escolha um dos dois para fazer o deploy.

Temos o guia da instalação [aqui](https://istio.io/docs/setup/kubernetes/).

Para fazer um exemplo, temos a aplicação de exemplo. Primeiro edite para NodePort no ingress:

```
kubectl edit svc istio-ingress -n istio-system # change loadbalancer to nodeport (or use hostport)
```

Depois, rode a aplicação de exemplo.

```
export PATH="$PATH:/home/ubuntu/istio-0.7.1/bin"
kubectl apply -f <(istioctl kube-inject --debug -f samples/bookinfo/kube/bookinfo.yaml)
```

Para acessar, use: http://<ip>:<NodePort>/productpage

Os arquivos estão em *resources/istio/install/kubernetes/*.

## Traffic Management

![](media/Screenshot-from-2019-05-31-16.25.04.png)

Vamos supor que existam dois serviços: A e B. E o serviço A leva informações para o serviço B.

Em um certo dia, houve um update no serviço B e você deseja testar para ver se a comunicação das informações não foi afetada. Istio pode fazer esse roteamento para você realizar os testes.

Pode ser feito para todos os users, para uma porcentagem ou alguma medida custom.

Primeiramente temos que criar um *Virtual Service*. Um para cada um dos microserviços. E vamos criar rotas para cada um deles:

```
istioctl create -f samples/bookinfo/routing/route-rule-all-v1.yaml
```

Esse arquivo está dentro do istio0.7;
Dentro dele, usamos Labels que fazem um match com o deploymento do Istio.

Para fazer um outro roteamento:

```
istioctl replace -f samples/bookinfo/routing/route-rule-reviews-test-v2.yaml
```

Com isso, podemos fazer um roteamento baseado em cookies.

Para fazer um roteamento baseado em porcentagem de tráfego:

```
istioctl replace -f samples/bookinfo/routing/route-rule-reviews-50-v3.yaml
```

Temos os arquivos nos *resources/istio/samples/bookinfo/routing/*.

## Distributed tracing

Podemos fazer [distributed tracing](https://opentracing.io/docs/overview/what-is-tracing/) usando duas ferramentas. 

A primeira é Zipkin.
Para instalar:

```
kubectl apply -f install/kubernetes/addons/zipkin.yaml
```

Depois, faça uma edição no serviço:

```
kubectl edit svc zikpin -n istio-system
```

E troque de ClusterIP para NodePort.

E poderemos acessar o Istio com a porta do serviço. E assim poderemos fazer o debug de microserviços.

A outra é o Jaeger

Para instalar:

```
kubectl apply -n istio-system -f https://raw.githubusercontent.com/jaegertracing/jaeger-kubernetes/master/all-in-one/jaeger-all-in-one-template.yml
```

É preciso deletar o Zipkin, caso esteja habilitado.

```
kubectl delete -f install/kubernetes/addons/zipkin.yaml
```

Faça uma edição no serviço:

```
kubectl edit svc jaeger-query -n istio-system
```

E troque de ClusterIP para NodePort.