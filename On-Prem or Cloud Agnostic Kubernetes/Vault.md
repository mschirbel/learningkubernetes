# Vault

É uma ferramenta feita pela Hashicorp e é usada para guardar senhas, keys e certificados.
Tudo pode ser criptografado. E é bem dinâmico e flexível.

Os secrets tem um TTL, e quando acaba são removidos.

## Vault Operator

Fica fácil fazer o deploy do Vault no K8s.
Permite que usemos a API do K8s para controlar o vault e é uma ótima alternativa para ferramentas assim nas public clouds.

## Demo

Temos os arquivos em *resources/vault/*. 

Primeiramente temos que fazer o deploy do etcd(banco de dados distribuidos para key-value no k8s). Os arquivos peguei do github do Vault.

```
kubectl create -f etcd-rbac.yaml 
kubectl create -f etcd_crds.yaml
kubectl create -f etcd-operator-deploy.yaml
```

```
kubectl get pods
```

Depois podemos fazer a instalação do Vault:

```
kubectl create -f vault-rbac.yaml 
kubectl create -f vault_crd.yaml
kubectl create -f vault-deployment.yaml
```

E agora podemos criar um cluster de vault + etcd:

```
kubectl create -f example_vault.yaml
```

Veja se foi criado:

```
watch kubectl get pods
```

Agora vamos acessar nosso Vault, para isso, instale a CLI:

```
wget https://releases.hashicorp.com/vault/0.10.1/vault_0.10.1_linux_amd64.zip
sudo apt-get -y install unzip
unzip vault_0.10.1_linux_amd64.zip
chmod +x vault
sudo mv vault /usr/local/bin
```

Agora vamos iniciar nosso cluster:

```
kubectl get vault example -o jsonpath='{.status.vaultStatus.sealed[0]}' | xargs -0 -I {} kubectl -n default port-forward {} 8200
```

Para usar certificados auto assinados:

```
export VAULT_ADDR='https://localhost:8200'
export VAULT_SKIP_VERIFY="true"
```

Agora podemos iniciar:

```
vault status
vault operator init
```

E receberemos uma key para o acesso inicial. E algumas keys para acessar o vault.

O vault inicia com 5keys para desbloqueio, mas precisa de no mínimo 3 para o acesso:

```
vault operator unseal <alguma das 5 keys>
vault operator unseal <alguma das 5 keys>
vault operator unseal <alguma das 5 keys>
```

```
vault login <key de login>
```

Se não fizemos o unseal, todos os dados são perdidos.
E podemos criar agora um secret.

```
kubectl -n default get vault example -o jsonpath='{.status.vaultStatus.active}' | xargs -0 -I {} kubectl -n default port-forward {} 8200
vault write secret/myapp/mypassword value=pass123
```

Para ler um secret:

```
vault read secrect/myapp/mypassword
```



Para usar o vault de dentro de alguma aplicação é necessário criar um token para o acesso:

```
vault write sys/policy/my-policy policy=@policy.hcl
vault token create -policy=my-policy
```

E para usar o vault via API com o token:

```
kubectl run --image ubuntu -it --rm ubuntu
apt-get update && apt-get -y install curl
curl -k -H 'X-Vault-Token: <token>' https://example:8200/v1/secret/myapp/mypassword
```