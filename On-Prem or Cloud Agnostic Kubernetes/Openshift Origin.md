# Openshift Origin

Openshift Origin é uma distribuição do Kubernetes.
É otimizado para desenvolvimento contínuo de aplicações e múltiplos desenvolvedores ao mesmo tempo.

Openshift Origin é o projeto da comunidade do Openshift(RedHat).

Usa Kubernetes, Docker e Project Atomic. Pode ser visto [aqui](https://www.okd.io/).

## Architecture

![](media/oc-arch.png)

O developer pode usar um versionador para guardar o código. O Openshift vai fazer a parte de CI/CD para subir o código nos Nodes.

Existem dois conceitos de Nodes:

1. Node - onde fica a aplicação
2. Infra Node - onde ficam os proxies, registries e etc.

Para rodar o OC Origin, podemos usar um RHEL ou Atomic Host.

Os arquivos estão em *resources/openshift/*.

E veja que na arquitetura temos como usar alguns Operators assim como vimos no vanilla Kubernetes.

Podemos integrar muito bem com Jenkins para fazer a CI/CD para o desenvolvedor. Ou seja, todas as partes de infra ficam mascaradas, da forma como o Openshift resolveu o problema.

## Install

Primeiramente, precisamos instalar o Docker. 
Aqui vamos usar um CentOS/7:

```
yum -y update
yum -y install docker
systemctl enable docker
systemctl start docker
```

E precisamos permitir o uso de registries inseguros.

```
echo '{
   "insecure-registries": [
     "172.30.0.0/16"
   ]
}' > /etc/docker/daemon.json
systemctl daemon-reload
systemctl restart docker
```

Esse IP é o registry que o Openshift usa.

E para instalar o OC:

```
curl -o ~/openshift-origin-client-tools-v3.9.0-191fece-linux-64bit.tar.gz -L https://github.com/openshift/origin/releases/download/v3.9.0/openshift-origin-client-tools-v3.9.0-191fece-linux-64bit.tar.gz
```

Agora extraia

```
tar -xzvf openshift-origin-client-tools-v3.9.0-191fece-linux-64bit.tar.gz
```

Deixe o binário no Path:

```
export PATH=$PATH:~/openshift-origin-client-tools-v3.9.0-191fece-linux-64bit
echo 'export PATH=$PATH:~/openshift-origin-client-tools-v3.9.0-191fece-linux-64bit' >> .bash_profile
```

Ou pode simplesmente mover para o */usr/bin/*. 
Agora, vamos subir o cluster:

```
oc cluster up --public-hostname=$(curl -s ifconfig.co) --host-data-dir=/data
```

Esse comando vai pegar o IP da nossa máquina e persistir quaisquer modificações no diretório */data*

## Run an App

Assim, vai ser fornecido um login e uma senha. Bem como a URL de login.

Nessa console, podemos escolher por algumas aplicações que estão no catálogo, ou podemos fazer o deploy de uma imagem Docker, ou até mesmo de arquivos YAML para o K8s.

Basta criar um projeto e criar uma app:

![](media/oc-create-project.png).