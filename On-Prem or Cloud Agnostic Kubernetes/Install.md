# Install Kubernetes

Vamos usar o *kubeadm*. Uma ferramenta feita pelo K8s para criar um cluster.
Funciona em qualquer Linux OS que tenha disponibildiade para pacotes *dev/rpm*.

*kubeadm* é ótimo pois tem várias ferramentas específicas de OS/Cloud. Mas ainda está em beta em algumas funcionalidades.

*kubeadm* utiliza *tokens* para realizar tarefas, como criação de um cluster ou fazer o join de nodes.

O formato de um *token* é <prefixo>.<seq. numeros><sufixo>.

Um contra-ponto é o fato de não instalar uma solução para a network. Aqui, usaremos a CNI(Container Network Interface) para rodar os comandos com kubectl apply.

Vou fazer a instalação em Ubuntu 18.04.
Caso queira ver como é feita em CentOS/7, clique [aqui](https://randomopenstackthoughts.wordpress.com/2019/01/26/install-kubernetes-v1-13-with-docker-18-06-on-centos-7/).

## Pré requisitos

    - Disponibilidade com deb/rpm packages
    - 2GB RAM
    - 2 CPUs(para o Master)
    - Conectividade entre os nodes
    - Mínimo de 2 nodes. Um master e um worker.

## Install Docker and Kubernetes

Primeiramente, precisamos instalar o Docker e o Kubernetes.

Caso seu sistema seja kubeadmin, veja os scripts em *resources/scripts/*.

Para instalar o Docker:

```sh
echo "installing docker"
apt update -y
apt install apt-transport-https ca-certificates curl gnupg software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt install docker.io -y
systemctl enable docker
systemctl start docker
docker -v
```

Pode acontecer do Docker vir com o cgroupfs como driver de cgroup. Mas é interessante trocar para o systemd:

```
cat << EOF > /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"]
}
EOF
systemctl restart docker
docker info | grep -i cgroup
```

Para instalar o Kubernetes:

```sh
echo "installing kubeadm and kubectl"
apt-get update && apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get update
apt-get install -y kubelet kubeadm kubectl
sudo systemctl enable kubelet
``` 

Agora, no nosso node Master, temos que inicializar nosso cluster.

## Initializing a Kubernetes Cluster

O primeiro passo, é definir um range de IP que será visto pelo node Master, e assim iniciaremos nosso cluster.

Caso sua rede esteja atrás de um firewall, você deve passar um range que seja permitido, como por exemplo:

```
kubeadm init --pod-network-cidr=10.244.0.0/16
```

Caso não esteja:

```
kubeadm init --pod-network-cidr=192.168.0.0/16
```

Caso o seu servidor de API tenha um outro IP, adicione o parâmetro

```
--apiserver-advertise-address="ip"
```

Pode acontecer do seu sistema necessitar de desligar a memória swap, para isso:

```
swapoff -a
```

O erro dado para isso é: *"[ERROR Swap]: running with swap on is not supported. Please disable swap"*. Podemos ver um [issue](https://github.com/kubernetes/kubeadm/issues/610) sobre isso.

Podemos receber um WARNING sobre o não uso de *systemd* como manager de cgroups para o Docker, isso pode ser visto [aqui](https://github.com/kubernetes/kubeadm/issues/1394).

Agora, precisamos exportar uma variável de ambiente *KUBECONFIG*:

``` sh
export KUBECONFIG=/etc/kubernetes/admin.conf
```

E vamos instalar o Calico:

```
kubectl apply -f https://docs.projectcalico.org/v3.1/getting-started/kubernetes/installation/hosted/canal/rbac.yaml
kubectl apply -f https://docs.projectcalico.org/v3.1/getting-started/kubernetes/installation/hosted/canal/canal.yaml
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/bc79dd1505b0c8681ece4de4c0d86c5cd2643275/Documentation/kube-flannel.yml
```

Com isso, nosso node Master irá gerar um comando para fazer o *join* do Node no cluster. Copie esse comando, e rode na máquina Node.

Caso não funcione, use o comando abaixo para ver as logs do sistema:

```
sudo ls -la  /etc/systemd/system/kubelet.service.d/
sudo cat /etc/systemd/system/kubelet.service.d/*.conf
sudo journalctl -xeu kubelet
```

Para receber o comando de join:

```
kubeadm token create --print-join-command
```

Se ao rodar o comando de join, ficar tudo esperando, veja as logs.
Caso seja um problema de *cni config uninitualized*, veja [aqui](https://github.com/kubernetes/kubernetes/issues/48798).

Depois de feito isso, verifique se o Docker está instalado:

```
docker ps
docker run hello-world
```

E veja se o Cluster reconheceu os nodes:

```
kubectl get nodes
```

## Adding a user

Pode ser necessário adicionar um usuário para administração do cluster:

```sh
groupadd kubeadmin
useradd -g kubeadmin -G sudo -s /bin/bash -d /home/kubeadmin kubeadmin
mkdir -p /home/kubeadmin
cp -r /root/.ssh /home/kubeadmin/.ssh
chown -R kubeadmin:kubeadmin /home/kubeadmin
echo "kubeadmin ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers
```

## Testing installation

Teste o comando com o usuário criado e veja se ele tem acesso aos nodes:

```
kubectl run helloworld --image=k8s.gcr.io/echoserver:1.4 --port8080
```

Agora verifique se foi criiado:

```
kubectl get deployments
```

Faça um expose do deployment:

```
kubectl expose deployment helloworld --type=NodePort
```

Isso vai criar um serviço criado.

```
curl <node IP>:31117/
curl <node IP>:31117/batata
```

## Restart the node

```sh
systemctl daemon-reload
systemctl restart docker
systemctl restart kubelet
```