# Calico

O Calico providencia segurança na conectividade de containers e máquinas virtuais. Calico é um SDN(Software Defined Network), com isso, podemos fazer a comunicação entre Pods em **diferentes** Nodes.

Assim teremos uma rede somente para a comunicação entre os Pods. Isso é uma CNI, Container Network Interface.

Calico cria uma camada 3 de rede usando BGP(Border Gateway Protocol). É altamente escalável.
Usa *etcd*, assim como o k8s. E funciona na AWS,GCP e outros. Funciona até mesmo nos serviços específicos do Kubernetes.

![](media/calico.png)

Temos um database, o etcd, e podemos usar o *calicoctl* para comunicar com esse db.

Em cada Node teremos um *daemon* chamado Felix, que irá no Kernel para fazer a configuração de routing e iptables. Assim, teremos a rede seja nos containers ou fora deles.

BGP Cliente é o que lê as rotas e distribui essa informação entre os Nodes. Deixando todos os Nodes atentos que o tráfego está sendo roteado.

BGP Route Reflector é um ponto central para os Clients do BGP saberem as rotas. É somente usado quando temos muitos nodes.

E dentro do Calico podemos configurar policies dentro da nossa rede. Para bloquear e permitir comunicação entre pods.

*Por definição, todos os pods aceitam tráfego de qualquer fonte.*

Nossos arquiivos estão em *resources/calico/*

O Calico já está instalado quando fizemos o setup do cluster:

```
kubectl apply -f https://docs.projectcalico.org/v3.1/getting-started/kubernetes/installation/hosted/canal/rbac.yaml
kubectl apply -f https://docs.projectcalico.org/v3.1/getting-started/kubernetes/installation/hosted/canal/canal.yaml
```

Agora, podemos começar com o serviço de nginx:

```
kubectl create -f nginx.yml
kubectl create -f networkpolicy-isolation.yml
kubectl create -f networkpolicy-nginx.yml
```

Nosso nginx tem a seguinte configuração:

```yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: nginx
spec:
  replicas: 3
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - name: http-port
          containerPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: nginx
  labels:
    app: nginx
spec:
  ports:
  - port: 80
    nodePort: 31001
    targetPort: http-port
    protocol: TCP
  selector:
    app: nginx
  type: NodePort
```

Podemos tentar alguma conexão vinda de outro pod:

```
kubectl run -it --rm -l app=access-nginx --image busybox busybox
wget nginx -O-
```

Mas veremos que isso não funciona. Precisamos configurar a nossa rede para aceitar isso.

Como por exemplo:

```yaml
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  name: access-nginx
  namespace: default
spec:
  podSelector:
    matchLabels:
      app: nginx
  ingress:
    - from:
      - podSelector:
          matchLabels:
            app: access-nginx
      ports:
      - protocol: TCP
        port: 80
```

Assim, essa restrição será feita em cima dos pods com a tag *app: nginx*. E os pods com a tag *app: access-nginx* poderão fazer o acesso.

```
kubectl create -f networkpolicy-nginx.yml
```

E tentamos novamente:

```
kubectl run -it --rm -l app=access-nginx --image busybox busybox
wget nginx -O-
```

Pode ver que sem o label não funciona:

```
kubectl run -it --rm -l --image busybox busybox
wget nginx -O-
```

## Deny Egress

Para fazer o deny de todo e qualquer egress de um pod:

```yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: default-deny
spec:
  podSelector: {}
  policyTypes:
  - Ingress
  - Egress
```

```
kubectl replace -f networkpolicy-isolation-egress.yml
kubectl run -it --rm -l app=access-nginx --image busybox busybox
wget google.com
```

E veja que não funciona pois bloqueamos toda a saída.

Mas se você quiser permitir algumas:

```yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-google
spec:
  podSelector: 
    matchLabels:
      app: egress
  policyTypes:
  - Egress
  egress:
  - to:
    - ipBlock:
        cidr: 8.8.8.8/32
---
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-dns
spec:
  podSelector: 
    matchLabels:
      app: egress
  policyTypes:
  - Egress
  egress:
  - to:
    # allow DNS resolution
    ports:
      - port: 53
        protocol: UDP
      - port: 53
        protocol: TCP
```

Assim, permitimos sair para o IP 8.8.8.8, do Google. Ou podemos permitir uma porta e um tipo de protocolo, como 53/UDP e 53/TCP.

E veja que só é possível graças a tag *app:egress*.

```
kubectl replace -f networkpolicy-allow-egress.yml
kubectl run -it --rm -l app=access-egress --image busybox busybox
wget google.com
```