# Dex

É um serviço de Identificação. Usa o OpenID Connect para autenticar os usuários.
Dex usa conectores para autenticar um usuário de um outro Provedor de Identificação.

Ou seja, é possível usar o Dex para se comunicar com o LDAP, SAML, Github, Microsoft e etc.

Diremos ao K8s para usar o OpenID Connect, este vai se comunicar com o Dex que vai fazer a ponte com o Provedor externo, seja ele qual for.

Dex então fornece um token para o usuário que é reconhecido pelo K8s. O k8s só enxerga o OIDC, por isso precisamos do Dex para fazer essa ponte.

Podemos ver mais sobre o processo de autenticação [aqui](https://kubernetes.io/docs/reference/access-authn-authz/authentication/)

![](https://d33wubrfki0l68.cloudfront.net/d65bee40cabcf886c89d1015334555540d38f12e/c6a46/images/docs/admin/k8s_oidc_login.svg)

Primeiro o usuário loga usando o Provedor externo. Caso isso dê OK, ele manda para o K8s um *access_token*, *id_token* e um *refresh_token*. Com esse token em mão, o usuário pode fazer requests a API do k8s, como por exemplo um *kubectl get pods*. O comando, antes de chegar no servidor da API, vai ser reconhecido pela autenticação do usuário. Depois o servidor de API verifica se a assinatura do token é **válida** e **não expirada**. Depois verifica se o usuário tem direitos de efeturar aquele comando. Se sim, retorna o output do comando.

## Login with Github

Podemos ver os arquivos necessários em *resources/dex*.

Primeiramente precisamos instalar o Dex. E é necessário um novo certificado para isso.

Para gerar o certificado temos o script *gencert.sh*:

```sh
#!/bin/bash

mkdir -p ssl

cat << EOF > ssl/req.cnf
[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name

[req_distinguished_name]

[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = dex.newtech.academy
EOF

openssl genrsa -out ssl/ca-key.pem 2048
openssl req -x509 -new -nodes -key ssl/ca-key.pem -days 10 -out ssl/ca.pem -subj "/CN=kube-ca"

openssl genrsa -out ssl/key.pem 2048
openssl req -new -key ssl/key.pem -out ssl/csr.pem -subj "/CN=kube-ca" -config ssl/req.cnf
openssl x509 -req -in ssl/csr.pem -CA ssl/ca.pem -CAkey ssl/ca-key.pem -CAcreateserial -out ssl/cert.pem -days 10 -extensions v3_req -extfile ssl/req.cnf
```

A única que precisamos trocar é a linha ```DNS.1 = dex.newtech.academy```. Se for um DNS não-comprado, aponte no */etc/hosts*.

Depois de criado o certificado, devemos importá-lo como um secret:

```
./gencert.sh
kubectl create -f dex-ns.yaml
kubectl create secret tls dex.newtech.academy.tls -n dex --cert=ssl/cert.pem --key=ssl/key.pem
sudo cp ssl/ca.pem /etc/kubernetes/pki/openid-ca.pem
```

Isso também vai criar o dex, que pode ser visto abaixo:

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: dex
```

Agora, criamos um secret para o Github. Para conseguir o *GITHUB_CLIENT_ID* e o *GITHUB_CLIENT_SECRET* devemos logar na conta do Github e criar uma organização. Depois entre nas configurações dessa organização e no fim do menu clique em **OAuth Apps**. Isso irá gerar uma URL de callback, com o seu domínio e uma porta estática. Por fim, teremos os IDs necessários. Exporte como variáveis de ambiente.

```
kubectl create secret \
    generic github-client \
    -n dex \
    --from-literal=client-id=$GITHUB_CLIENT_ID \
    --from-literal=client-secret=$GITHUB_CLIENT_SECRET
```

Veja como ficou o secret:

```
kubectl edit secret github-client -n dex
```

Agora, vamos adicionar o arquivo de manifest do API Server. Ou seja, vamos falar para o servidor de API que vamos usar OIDC.

```
    - --oidc-issuer-url=https://dex.newtech.academy:32000
    - --oidc-client-id=example-app
    - --oidc-ca-file=/etc/kubernetes/pki/openid-ca.pem
    - --oidc-username-claim=email
    - --oidc-groups-claim=groups
```

Os arquivos de manifests estão em */etc/kubernetes/manifests/kube-apiserver.yaml*. Não use VIM para editar esse arquivo. Use *Nano* ou *Emacs*.

Para ver se o servidor de API está respondendo as mudanças:

```
docker ps -a |grep kube-apiserver
```

Mas ainda podemos logar, pois estamos usando certificados. E quando usamos certificados não precisamos de nenhum método de autenticação.

Vamos fazer o deploy do nosso Dex:

```
kubectl create -f dex.yaml
```

Podemos ver o arquivo:

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: dex
  namespace: dex
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  name: dex
  namespace: dex
rules:
- apiGroups: ["dex.coreos.com"] # API group created by dex
  resources: ["*"]
  verbs: ["*"]
- apiGroups: ["apiextensions.k8s.io"]
  resources: ["customresourcedefinitions"]
  verbs: ["create"] # To manage its own resources identity must be able to create customresourcedefinitions.
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: dex
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: dex
subjects:
- kind: ServiceAccount
  name: dex                 # Service account assigned to the dex pod.
  namespace: dex  # The namespace dex is running in.
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  labels:
    app: dex
  name: dex
  namespace: dex
spec:
  replicas: 3
  template:
    metadata:
      labels:
        app: dex
    spec:
      serviceAccountName: dex
      hostAliases:
      - ip: "127.1.2.3"
        hostnames:
        - "ldap01.example.com"
      containers:
      - image: quay.io/coreos/dex:v2.10.0
        name: dex
        command: ["/usr/local/bin/dex", "serve", "/etc/dex/cfg/config.yaml"]

        ports:
        - name: https
          containerPort: 5556

        volumeMounts:
        - name: config
          mountPath: /etc/dex/cfg
        - name: tls
          mountPath: /etc/dex/tls
        - name: ldap-tls
          mountPath: /etc/dex/ldap-tls

        env:
        - name: GITHUB_CLIENT_ID
          valueFrom:
            secretKeyRef:
              name: github-client
              key: client-id
        - name: GITHUB_CLIENT_SECRET
          valueFrom:
            secretKeyRef:
              name: github-client
              key: client-secret
      volumes:
      - name: config
        configMap:
          name: dex
          items:
          - key: config.yaml
            path: config.yaml
      - name: tls
        secret:
          secretName: dex.newtech.academy.tls
      - name: ldap-tls
        configMap:
          name: ldap-tls
          items:
          - key: cacert.pem
            path: cacert.pem
---
kind: ConfigMap
apiVersion: v1
metadata:
  name: ldap-tls
  namespace: dex
data:
  cacert.pem: |
    empty
---
kind: ConfigMap
apiVersion: v1
metadata:
  name: dex
  namespace: dex
data:
  config.yaml: |
    issuer: https://dex.newtech.academy:32000
    storage:
      type: kubernetes
      config:
        inCluster: true
    web:
      https: 0.0.0.0:5556
      tlsCert: /etc/dex/tls/tls.crt
      tlsKey: /etc/dex/tls/tls.key
    connectors:
    - type: github
      id: github
      name: GitHub
      config:
        clientID: $GITHUB_CLIENT_ID
        clientSecret: $GITHUB_CLIENT_SECRET
        redirectURI: https://dex.newtech.academy:32000/callback
        org: kubernetes
    oauth2:
      skipApprovalScreen: true

    staticClients:
    - id: example-app
      redirectURIs:
      - 'https://dex.newtech.academy:32000/callback'
      - 'http://178.62.90.238:5555/callback'
      name: 'Example App'
      secret: ZXhhbXBsZS1hcHAtc2VjcmV0
    enablePasswordDB: false
---
apiVersion: v1
kind: Service
metadata:
  name: dex
  namespace: dex
spec:
  type: NodePort
  ports:
  - name: dex
    port: 5556
    protocol: TCP
    targetPort: 5556
    nodePort: 32000
  selector:
    app: dex
```

Não esqueça de trocar o DNS. E a linha *'http://178.62.90.238:5555/callback'* se refere ao IP do Master.

Agora vamos ver um exemplo que *COREOS* nos dá para um frontend de exemplo:

```
sudo apt-get install make golang-1.9
git clone https://github.com/coreos/dex.git
cd dex
git checkout v2.10.0
export PATH=$PATH:/usr/lib/go-1.9/bin
go get github.com/coreos/dex
make bin/example-app
export MY_IP=$(curl -s ifconfig.co)
./bin/example-app --issuer https://dex.newtech.academy:32000 --issuer-root-ca /etc/kubernetes/pki/openid-ca.pem --listen http://${MY_IP}:5555 --redirect-uri http://${MY_IP}:5555/callback
```

E agora vamos criar um usuário para listar os Pods.
O arquivo user.yaml:

```yaml
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: Role
metadata:
  name: exampleUser
  namespace: default
rules:
- apiGroups: [""] # "" indicates the core API group
  resources: ["pods"]
  verbs: ["get", "watch", "list"]
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: RoleBinding
metadata:
  name: exampleUser
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: exampleUser
subjects:
- kind: User
  name: your@email.inv
  namespace: default
```

E agora vamos setar as credenciais para logar na app. Após criar o usuário, e logar na página, vc receberá um token, que será usado na variável de ambiente *TOKEN*.

```
kubectl create -f user.yaml
#kubectl config set-credentials developer --token ${TOKEN}
kubectl config set-credentials developer --auth-provider=oidc --auth-provider-arg=idp-issuer-url=https://dex.newtech.academy:32000 --auth-provider-arg=client-id=example-app --auth-provider-arg=idp-certificate-authority=/etc/kubernetes/pki/openid-ca.pem  --auth-provider-arg=id-token=${TOKEN}
kubectl config set-context dev-default --cluster=kubernetes --namespace=default --user=developer
kubectl config use-context dev-default
```

E ao tentar fazer um ```kubectl get nodes``` será impedido pois o usuário não tem permissão de acordo com a Role criada.

Mas funciona com ```kubectl get pods```.

## Login with LDAP

Nos arquivos em *resources/dex/* temos os arquivos necessários.

Primeiramente, precisamos instalar o LDAP:

```
sudo apt-get -y install slapd ldap-utils gnutls-bin ssl-cert
sudo dpkg-reconfigure slapd
./gencert-ldap.sh
```

Nesse script *gencert-ldap*:

```
#!/bin/bash

# from https://help.ubuntu.com/lts/serverguide/openldap-server.html

set -x

sudo sh -c "certtool --generate-privkey > /etc/ssl/private/cakey.pem"

echo 'cn = Example Company
ca
cert_signing_key
' > /tmp/ca.info

sudo mv /tmp/ca.info /etc/ssl/ca.info

sudo certtool --generate-self-signed \
--load-privkey /etc/ssl/private/cakey.pem \
--template /etc/ssl/ca.info \
--outfile /etc/ssl/certs/cacert.pem

sudo certtool --generate-privkey \
--bits 1024 \
--outfile /etc/ssl/private/ldap01_slapd_key.pem

echo 'organization = Example Company
cn = ldap01.example.com
tls_www_server
encryption_key
signing_key
expiration_days = 3650' > /tmp/ldap01.info

sudo mv /tmp/ldap01.info /etc/ssl/ldap01.info

sudo certtool --generate-certificate \
--load-privkey /etc/ssl/private/ldap01_slapd_key.pem \
--load-ca-certificate /etc/ssl/certs/cacert.pem \
--load-ca-privkey /etc/ssl/private/cakey.pem \
--template /etc/ssl/ldap01.info \
--outfile /etc/ssl/certs/ldap01_slapd_cert.pem

sudo chgrp openldap /etc/ssl/private/ldap01_slapd_key.pem
sudo chmod 0640 /etc/ssl/private/ldap01_slapd_key.pem
sudo gpasswd -a openldap ssl-cert

sudo sh -c "cat /etc/ssl/certs/cacert.pem >> /etc/ssl/certs/ca-certificates.crt"

sudo systemctl restart slapd.service
```

Com esse script vamos criar um certificaddo para esse hostname e usar no LDAP.

E agora configuramos para o LDAP reconhecer os nossos certificados. E adicionar um usuário.:

```
sudo ldapmodify -H ldapi:// -Y EXTERNAL -f ldap/certinfo.ldif
ldapadd -x -D cn=admin,dc=example,dc=com -W -f ldap/users.ldif 
```

Agora precisamos adicionar o *ldaps*:

/etc/default/slapd:
```
SLAPD_SERVICES="ldap:/// ldapi:/// ldaps:///"
```
e rode:

```
sudo systemctl restart slapd.service
```

E adicione a URL *ldap01.example.com* no hosts da máquina e teste a URL:

```
ldapsearch -xD 'uid=serviceaccount, ou=People,dc=example,dc=com' -w 'serviceaccountldap' -H ldap://ldap01.example.com -ZZ -b dc=example,dc=com 'uid=john' cn gidNumber
```

E vamos adicionar o nosso config map:

```
kubectl config use-context kubernetes-admin@kubernetes
cat /etc/ssl/certs/cacert.pem
```

Pegue o conteudo do certificado e cole no config map

```
kubectl edit configmap ldap-tls -n dex
```

Após editado com o certificado

```
kubectl apply -f configmap-ldap.yaml
kubectl edit deploy dex -n dex  # edit the ldap IP alias
```

O novo configmap:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: dex
  namespace: dex
data:
  config.yaml: |
    issuer: https://dex.newtech.academy:32000
    storage:
      type: kubernetes
      config:
        inCluster: true
    web:
      https: 0.0.0.0:5556
      tlsCert: /etc/dex/tls/tls.crt
      tlsKey: /etc/dex/tls/tls.key
    connectors:
    - type: github
      id: github
      name: GitHub
      config:
        clientID: $GITHUB_CLIENT_ID
        clientSecret: $GITHUB_CLIENT_SECRET
        redirectURI: https://dex.newtech.academy:32000/callback
        org: kubernetes
    - type: ldap
      id: ldap
      name: LDAP
      config:
        host: ldap01.example.com:636
        rootCA: /etc/dex/ldap-tls/cacert.pem

        # The DN and password for an application service account. The connector uses
        # these credentials to search for users and groups. Not required if the LDAP
        # server provides access for anonymous auth.
        # Please note that if the bind password contains a `$`, it has to be saved in an
        # environment variable which should be given as the value to `bindPW`.
        bindDN: uid=serviceaccount,ou=People,dc=example,dc=com
        bindPW: serviceaccountldap

        # The attribute to display in the provided password prompt. If unset, will
        # display "Username"
        usernamePrompt: SSO Username

        # User search maps a username and password entered by a user to a LDAP entry.
        userSearch:
          # BaseDN to start the search from. It will translate to the query
          # "(&(objectClass=person)(uid=<username>))".
          baseDN: ou=People,dc=example,dc=com
          # Optional filter to apply when searching the directory.
          filter: "(objectClass=inetOrgPerson)"

          # username attribute used for comparing user entries. This will be translated
          # and combined with the other filter as "(<attr>=<username>)".
          username: uid
          # The following three fields are direct mappings of attributes on the user entry.
          # String representation of the user.
          idAttr: uid
          # Required. Attribute to map to Email.
          emailAttr: mail
          # Maps to display name of users. No default value.
          nameAttr: cn

        # Group search queries for groups given a user entry.
        groupSearch:
          # BaseDN to start the search from. It will translate to the query
          # "(&(objectClass=group)(member=<user uid>))".
          baseDN: ou=Groups,dc=example,dc=com
          # Optional filter to apply when searching the directory.
          filter: "(objectClass=group)"

          # Following two fields are used to match a user to a group. It adds an additional
          # requirement to the filter that an attribute in the group must match the user's
          # attribute value.
          userAttr: uid
          groupAttr: member

          # Represents group name.
          nameAttr: name
    oauth2:
      skipApprovalScreen: true
    staticClients:
    - id: example-app
      redirectURIs:
      - 'http://178.62.90.238:5555/callback'
      name: 'Example App'
      secret: ZXhhbXBsZS1hcHAtc2VjcmV0
    enablePasswordDB: false
```

E agora vamos deletar nossos pods do Dex:

```
kubectl delete pods -n dex --all
```

Eles vão ser construídos novamente graças ao deployment.

```
kubectl get pods -n dex
```

Para fazer um debug, escolha um pod e olhe as logs:

```
kubectl logs <pod> -n dex
```