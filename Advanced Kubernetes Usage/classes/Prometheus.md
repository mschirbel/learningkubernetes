# Monitoring with Prometheus

Prometheus é uma ferramenta de monitoração e alerta. Totalmente open-source.

Foi criado pela Soundcloud mas agora está com a CNCF.

Prometheus funciona baseado cm key/value pair e não precisa de storage distribuído. E faz o pull direto dos serviços usando HTTP.

Fora que, tem uma UI própria para análise de dadods.

Todos os arquivos estão em *resources/prometheus/*

## Arquitetura

![](../media/prometheus-arch.png)

Faremos o deploy de diversos serviços. E para cada um deles teremos um *ServiceMonitor*. Um recurso do K8s que permite que o Prometheus se conecte e capte as informações.

Depois teremos um PrometheusOperator que permite definir recursos para observar e analisar os dados coletados.

## Install

Primeiro, precisamos criar os recursos do RBAC:

```
kubectl create -f rbac.yml
```

Para fazer a instalação do Prometheus e dos seus recursos(aqui também tem um para alerta, mas não usaremos agora):
```
kubectl create -f prometheus.yml
kubectl create -f prometheus-resource.yml
```

Para criar o Kubernetes monitoring, isso se encarrega de acessar as métricas que o Prometheus precisa:
```
kubectl create -f kubernetes-monitoring.yml
```

E uma aplicação de exemplo(veja que temos um ServiceMonitor, assim como mostrado na arquitetura):
```
kubectl create -f example-application.yml
```

Para ver os Pods:

```
kubectl get pods
```

Veja que temos pods para o Prometheus:

```
kubectl get prometheus
```

E isso ocorre porque temos um stateful set:

```
kubectl get statefulset
```

Para ver o endpoint:

```
kubectl describe svc/prometheus
```

Para escrever as suas métricas, o Prometheus tem uma linguagem própria. Para saber mais, clique [aqui](https://prometheus.io/docs/querying/basics/).