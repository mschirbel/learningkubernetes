# Authentication

Por default são usados certificados **X509** para fazer a autenticação com a API. Sempre que usamos *kubectl* estamos usando esse certificado.

Os certificados são assinados assim que criamos o cluster.
Mas ao criar novos usuários temos que gerar novos certificados e assiná-los com a entidade usada pelo K8s.

## HTTP Basic Authentication

A API usa HTTP, então podemos usar uma autenticação de username e password.
Apesar de simples, não é seguro e é difícil de manter.

Podemos usar um arquivo estático contendo as credenciais. E assim passamos para a API o arquivo que queremos usar com o parâmetro:

```
--basic-auth-file=/path/to/file
```

O arquivo precisa estar no formato:

`password,user,uid,"group1","group2","group3"`

Esse método pode ser usado para uma pequena manutenção. Mas tem um downside: toda vez que um novo usuário for adicionado a API deve ser reiniciada.

E algumas ferramentas não suportam essa função.

## Auth with Proxy

Quando usamos um proxy, tiramos essa parte do K8s e escrevemos nosso próprio método de autenticação.

É uma boa solução caso não tenha suporte para a sua ferramenta.

O processo é o seguinte:

1. O proxy precisa de um certificado assinado pela entidade usada na API. Você deve usar o método de `--requestheader-clien-ca-file`.
2. O proxy faz a autenticação com qualquer mecanismo que for necessário.
3. Uma vez que autenticado, o proxy encaminha a request para a API com o HTTP header.
4. Ao iniciar a API você deve passar uma flag indicando o proxy que cuida da autenticação, `--requestheader-username-headers=X-Remote-User`,  `--requestheader-username-headers=X-Remote-Group` e `--requestheader-username-headers=X-Remote-Extra`
5. O **X-Remote-User** ,**X-Remote-Group** e **X-Remote-Extra**são usados como argumentos no HTTP header.

## OpenID Connect

OpenID é feito em cima de *OAuth2*.
Podemos fazer um autenticação após receber um token.

Esse token é verificado se realmente veio do servidor de autenticação, pois é assinado com uma Hash.

Esse token é um *JWT*, um JSON Web Token. Ele contém os dados de login do usuário.

Uma vez obtido, pode ser usado para fazer o login na API, com o parâmetro `kubectl get pods --token=token`.

Esse token também pode ser usado para logar na dashboard do K8s.

### Process of Authentication

Primeiro o usuário loga usando o Provedor externo. Caso isso dê OK, ele manda para o K8s um *access_token*, *id_token* e um *refresh_token*. Com esse token em mão, o usuário pode fazer requests a API do k8s, como por exemplo um *kubectl get pods*. O comando, antes de chegar no servidor da API, vai ser reconhecido pela autenticação do usuário. Depois o servidor de API verifica se a assinatura do token é **válida** e **não expirada**. Depois verifica se o usuário tem direitos de efeturar aquele comando. Se sim, retorna o output do comando.

## OpenID Connect with auth0

Passos:

1. Ter um Provedor de Identificação. No caso vamos usar o *auth0*.
2. Criar um client do *auth0* para o K8s
3. Editar o cluster com **oidc**.
4. Deploy de um servidor de autenticação(UI proxy + token manager)
5. Criar um DNS record para o *auth* server
6. Conectar na UI do K8s.

Para começar, entre no [site do auth0](https://auth0.com). Faça o login no site.
Na dashboard do site crie um novo client. Escolha uma Regular Web App e dê um nome.

Nas configurações: dê um domínio e gere um ID e um Secret. Também especifique uma Allowed Callback URLs. Essa URL deve ser usada no deploy no cluster, de preferência com algum contexto. E copie somente a URI para Allowed Logout URLs.

Também é necessário criar uma Database Connection. E nao habilite o Sign Up. E habilite essa conexão no Client que você criou. Depois podemos criar usuários usando o menu principal.

Salve as mudanças.

Agora vamos fazer o setup do cluster. Os arquivos estão em *resources/authentication/*.

Os arquivos para criar a UI estão em *resources/kubernetes-auth-server/*

Ao criar o cluster passe o parâmetro `--name=algo.dominio.com`. Sendo que isso deve corresponder com a URL deixada no site do auth0.

Agora edite o cluster para ter as seguintes informações:

```
spec:
  kubeAPIServer:
    oidcIssuerURL: https://account.eu.auth0.com/
    oidcClientID: clientid
    oidcUsernameClaim: sub
```

E troque *oidcIssuerURL* para o mesmo Domain deixado no site do Auth. Bem como o ClientID.

Agora faça o update do cluster.

Agora vamos criar a UI Dashboard para o Login:

```
kubectl create -f https://raw.githubusercontent.com/kubernetes/kops/master/addons/kubernetes-dashboard/v1.6.3.yaml
```

E vamos fazer o deploy do Authentication Server:

```
kubectl create -f resources/authentication/*
```

No arquivo *auth0-secrets.yml* troque o campo *AUTH0_CLIENT_SECRET* para o SecretID no site do Auth0. Mas o secret precisa estar em base64, para isso:

```
echo -n "secret do site"|base64
```

No arquivo *auth0-deployment.yml* temos a UI do nosso servidor. Ness arquivo troque as informações pelas mesmas preenchidas no site do Auth0. E também a conexão com o database que criamos no Auth0.

```
kubectl get pods.
```

Após isso, adicione mais um record set no seu domínio. Isso pode ser feito no seu provedor. Deve apontar para o IP usado no cluster, seja ele um LB ou não.

Caso queira logar diretamente pelo código python, na console do Auth0, na parte de Client, você deve habilitar o login por Password nas opções avançadas. 

Aí podemos criar um alias com as informações do comando:

```
alias kubectl="kubectl --token=\$(AUTH0_CLIENT_ID=<client_id> AUTH0_DOMAIN=<domain> APP_HOST=<host> /path/to/cli-auth.py)"
```

Assim quando rodarmos o comando nosso login será requisitado e mantido em cache por um tempo.