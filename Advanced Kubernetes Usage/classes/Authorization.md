# Authorization

Depois do usuário logar, a autorização controla aquilo que ele **pode** fazer. E até qual parte do sistema o usuário pode acessar.

Todo esse controle é feito na API. E existem diversos módulos:

1. Node: autorização para requests feitas por kubelets
2. ABAC: atribute-based access control - basear o acesso em diversas policies.
3. RBAC: role based access control - baseado em acesso granular em roles.
4. Webhook: é uma opção customizável para um servidor próprio de autorização.

RBAC é o que vamos usar, pois podemos controlar cada role e atribuir a grupos. É estável já no K8s.

Para habilitar o modo de autorização, precisamos passar `--authorization-mode` para o API ao iniciar, no caso do RBAC: `--authorization-mode=RBAC`.

Com o **kops** podemos criar um cluster com a flag `--authorization`. Se não usar nada, tudo é possível para todos.

Para editar o cluster para usar o modo de autorização, devemos editar as seguintes linhas:

```
kubeAPIServer:
    authorizationMode: RBAC
```

## RBAC

Podemos garantir permissões para o comando *kubectl*.

Criamos uma role no formato YAML e aplicamos no cluster. Depois, designamos essa role para algum usuário ou grupo. E assim podemos limitar namespaces, comandos e acessos a diferentes usuários ou grupos.

Existem dois tipos de roles:

1. aplicadas a uma namespace específica
2. aplicadas a todo o cluster

Podemos definir a role sobre quaisquer objetos do cluster, como por exemplo:

```yaml
kind: Role
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
    namespace: default
    name: pod-reader
rules:
- apiGroups: [""]
  resources: ["pods", "secrets"]
  verbs: ["get", "watch", "list"]
```

Nessa role, podemos ler e listar os pods e secrets dentro da default namespace. Mas essa role se aplica somente a uma namespace, para aplicar a todo o cluster:

```yaml
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
    name: pod-reader-cluster
rules:
- apiGroups: [""]
  resources: ["pods", "secrets"]
  verbs: ["get", "watch", "list"]
```

E para aplicar a Role de uma namespace a um usuário/grupo:

```yaml
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
    namespace: default
    name: pod-reader
subjects:
- kind: User
  name: bob
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: pod-reader
  apiGroup: rbac.authorization.k8s.io
```

E para aplicar uma Role de cluster a um usuário/grupo:

```yaml
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
    name: pod-reader
subjects:
- kind: User
  name: bob
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: pod-reader-cluster
  apiGroup: rbac.authorization.k8s.io
```

## RBAC with auth0

Temos a role que usaremos em *resources/authorization/*

Mas antes, temos que habilitar isso no API Server:

```
spec:
  kubeAPIServer:
    oidcIssuerURL: https://account.eu.auth0.com/
    oidcClientID: clientid
    oidcUsernameClaim: name
    oidcGroupsClaim: http://authserver.kubernetes.newtech.academy/claims/groups
  authorization:
    rbac: {}

```

Edite o cluster com essas informações. As informações estão na conta do auth0. E faça o update do cluster.

Agora precisamos configurar o auth0. No menu principal, va em Extensios e procure por *Auth0 Authorization* e instale.

Para configurar essa extensão, crie um novo grupo. E adicione algum usuário a ele.

Agora va em *Rules* e crie uma nova com a seguinte config:

```js
function (user, context, callback) {
  var namespace = 'http://authserver.kubernetes.newtech.academy/claims/'; // You can set your own namespace, but do not use an Auth0 domain

  // Add the namespaced tokens. Remove any which is not necessary for your scenario
  context.idToken[namespace + "permissions"] = user.permissions;
  context.idToken[namespace + "groups"] = user.groups;
  context.idToken[namespace + "roles"] = user.roles;
  
  callback(null, user, context);
}
```

Essa função adiciona um grupo a um token. E depois faça o link de DNS com o IP do cluster ou o LB criado.

Crie a Role:

```
kubectl create -f resources/authorization/role.yml
```

E ao tentar listar os nodes, não será permitido. Desde que, o grupo criado no auth0 seja o mesmo dessa role.

## Complex Role for RBAC

Podemos definir quais parâmetros são possíveis usar em quais comandos:

```
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
    name: complex-role
rules:
- apiGroups: [""]
  resources: ["configmaps", "secrets", "nodes"]
  verbs: ["get","list","watch"]
- apiGroups: ["batch"]
  resources: ["jobs"]
  verbs: ["get","list","watch","create","update","patch","delete"]  
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["get","list","watch"]
```

Podemos usar também as roles que são pré-definidas pelo RBAC. Podemos vê-las [aqui](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#default-roles-and-role-bindings).