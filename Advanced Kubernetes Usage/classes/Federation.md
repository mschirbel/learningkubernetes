# Federation

Federation pode ser usado pra controlar múltiplos cluster. Podemos fazer deploys em diferentes clusters e tudo do mesmo jeito. Assim sincronizamos os recursos.

Podemos usar até mesmo o mesmo DNS record entre os clusters. Assim podemos espalhar o load entre diversos clusters e temos mais disponibilidade.

Isso significa que podemos usar clustes em diferentes regions. Ou até mesmo em clouds diferentes.

A partir do K8s 1.5 podemos usar o `kubefed` para administrar os clusters.

## Install

Em *resources/federation/* temos métodos pra instalar em Linux, Windows e Mac.

Para linux:

```
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/kubernetes-client-linux-amd64.tar.gz
tar -xzvf kubernetes-client-linux-amd64.tar.gz
```

## Creating 2 clusters

```
kops create cluster --name=domain1 --state=s3://kops-state-b429b --zones=eu-west-1a --node-count=2 --node-size=t2.small --master-size=t2.small --dns-zone=domain1

kops create cluster --name=domain2 --state=s3://kops-state-b429b --zones=eu-west-1b --node-count=2 --node-size=t2.small --master-size=t2.small --dns-zone=domain2

kops update cluster domain1 --state=s3://kops-state-b429b --yes
kops update cluster domain2 --state=s3://kops-state-b429b --yes
```

*Não esqueça de fazer o export do profile da AWS*.

Para iniciar a federation:

Antes, crie um RecordSet no seu domínio para federated.<seu_dominio>. Mas deixe correto que a Hosted Zone ID seja acessível pelas Roles do Kubernetes (na AWS isso deve ser editado no IAM).

Para ver os pods dos clusters:

```
kubectl get nodes

# para ver os clusters
kubectl config get-contexts

# para trocar de cluster
kubectl config use-context <nome do outro cluster>
```

Agora inicie o Federation.

```
kubefed init federated --host-cluster-context=domain1 --dns-provider="aws-route53" --dns-zone-name="domain1"
```

E veja os seus contextos:

```
kubectl config get-contexts
```

Temos um novo, agora use o federated:

```
kubectl config use-context federated
```

E agora faça o join:

```
kubefed join kubernetes-2 --host-cluster-context=kubernetes.newtech.academy --cluster-context=kubernetes-2.newtech.academy
```

E faça o mesmo no outro cluster:

```
kubefed join kubernetes-1 --host-cluster-context=kubernetes.newtech.academy --cluster-context=kubernetes.newtech.academy
```

E crie uma nova namespace para esse Federation:

```
kubectl create namespace default --context=federated
```

Para fazer um deploy no Federation, use a flag `--context=federated`

```
kubectl --context=federated create -f helloworld-deployment.yml
kubectl --context=federated create -f helloworld-service.yml
```

Esses arquivos estão em *resources/federation/*.

Para ver os Pods em cada um dos clusters:

```
kubectl --context=domain1 get pods
kubectl --context=domain2 get pods
```

Veja que existem agora 4 replicas no arquivo e duas em cada um dos clusters.