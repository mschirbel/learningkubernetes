# Linkerd

Podemos ter diversos microserviços em nosso cluster. E pode ser complicado administrar todos os endpoints dele.

Há algumas falhas que vimos até agora:

1. *service discovery* é bem limitado
2. routing é sempre feito em round-robin
3. não há segurança contra falhas
4. é difícil visualizar todos os serviços

O que acontece quando uma appA manda um HTTP/GET para uma appB e esta está indisponível? A request falha e não há uma segunda tentativa.

Linkerd pode resolver esse problema. É um proxy que:

1. routing baseado em latência
2. faz canary deployment
3. failure handling
4. visibilidade de serviços

É ótimo para tracing e debugging.
Linkerd é usado com um DaemonSet em cada um dos nodes.

Então após a request do client, o Linkerd vai fazer o proxy até o service. A aplicação faz o contato diretamente o DaemonSet do Linkerd, que vai repassar a resposta para o Client.
Mas se a request falhar nesse node, ele pode enviar para um outro DaemonSet em outro node.

![](../media/linkerd-proxy.png)

Para o Pod encontrar o Linkerd:

```yaml
        - name: http_proxy
          value: $(NODE_NAME):4140
```

Assim como no arquivo *resources/linkerd/hello-world,yml*.

## Install

Primeiro, instalamos o Linkerd:

```
kubectl apply -f resources/linkerd/linkerd.yml
```

E para visualizar a url de Ingress:

```
INGRESS_LB=$(kubectl get svc l5d -o jsonpath="{.status.loadBalancer.ingress[0].*}")
echo http://$INGRESS_LB:9990
```
Na URL acima temos acesso aos requests incoming and outgoing


Para ter acesso a UI do Linkerd:

```
kubectl apply -f linkerd-viz.yml
VIZ_INGRESS_LB=$(kubectl get svc linkerd-viz -o jsonpath="{.status.loadBalancer.ingress[0].*}")
echo http://$VIZ_INGRESS_LB
```
Na URL acima temos acesso a uma visualização no Grafana.

Agora podemos criar a nossa app hello-world:

```
kubectl create -f resources/linkerd/hello-world.yml
```

Para visualizarmos a app hello:

```
http_proxy=$INGRESS_LB:4140 curl -s http://hello
```

Para visualizarmos a app world:

```
http_proxy=$INGRESS_LB:4140 curl -s http://world
```