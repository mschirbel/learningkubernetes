# Introduction

É necessário ter um conhecimento prévio, ainda que introdutório, em Kubernetes.

Para instalar o K8s, temos o arquivo em *resources/Install.md*.

Nesse curso vamos aprender sobre tópicos avançados, desde cluster até segurança.

No decorrer do material, vamos assumir que existe um cluster de Kubernetes com 1 Master e 2 Nodes.