# Deploy on Kubernetes with Spinnaker

Spinnaker é um software de CD. Pode fazer o deploy em diversos cloud providers, OpenStack e Kubernetes.

Foi criado pelo Netflix. Se integra com diversas ferramentas de CI e de Monitoração.

## Features

1. Cluster Management

Podemos ver os recursos na nuvem ou no cluster de K8s.

2. Deployment Management

Podemos criar workflows e pipelines de deploy.

## Deployment Strategies

1. Blue/Green Deployment

São chamados no Spinnaker de Red/Black.
Há dois grupos de instâncias, ligadas por um Lb. Quando uma atualização sai, um dos grupos é retirado do LB e é feito o deploy neste grupo. Após validar, esse grupo atualizado volta para o LB e o outro grupo sem a atualização sai.
Quando os dois estiverem atualizados, ambos voltam para o LB.

2. Rolling Red/Black

Um grupo com todas as instâncias e um grupo sem nenhuma. É escolhido um número *k* de instâncias para fazer o update. Essas instâncias são movidas para o grupo de update e validadas. Se ok, outras *k* instâncias são movidas para o grupo de update. Assim, até todas estarem completas.

3. Canary

É a mesma coisa do Rolling, mas ele funciona baseado num tempo mínimo para análise e é baseado em uma certa porcentagem de usuários.

---

Um exemplo do deploy é:

![](../media/spinnaker&#32;deploy.png)

### Key Concepts

- Account: são as credenciais usadas para logar em registries ou clusters
- Instance: é um pod do K8s
- Server Group: é um replica set
- Cluster: é um deployment do spinnaker
- LoadBalancer: é um service no K8s

## Deploy Example

![](../media/pipeline-spinnaker.png)

Após o commit, temos um trigger do Github para fazer a build e o push para o Dockerhub. Isso também faz um trigger para o nosso cluster do K8s.

## Install Spinnaker

Vamos usar o Helm para instalar:

```
helm init
helm install --name demo -f spinnaker.yml stable/spinnaker
```

O arquivo está em *resources/spinnaker/*.

Precisamos fazer algumas modificações no nosso arquivo *spinnaker.yml*. Caso você queira algum repositório no Dockerhub além dos defaults(como algum seu, por exemplo) é necessário adicionar isso nas accounts:

```yaml
accounts:
- name: dockerhub
  address: https://index.docker.io
  repositories:
    - library/alpine
    - library/ubuntu
    - library/centos
    - library/nginx
    - marceloschirbel/spinnaker
```

É possível configurar muitas outras coisas, mas isso está nas docs.

Pode demorar um pouco, pois o Spinnaker instala muitas coisas, inclusive o Jenkins.
Após a instalação, receberemos dois comandos para fazer um port fowarding para acessar a UI do Spinnaker.

## Link Dockerhub with Github

Nas configurações de conta do Dockerhub é possível linkar com o Github. Basta ir em Settings e em *Linked Accounts & Services*.

Agora podemos criar uma nova *Automated Build*, dentro do menu *Create*.
Selecione um repositório do Github e crie um repo no Dockerhub com o nome do repo no Github.

## Configure Spinnaker

Ao entrarmos na UI do Spinnaker e clicarmos em *Applications*, veremos as principais aplications do nosso cluster de K8s.

![](../media/s-apps.png)

Ao criarmos uma nova, basta dar um nome e clicar em create.

Primeiramente precisamos criar um LoadBalancer(service), depois um ServerGroup(replicaset) e por ultimo um Pipeline.

Para criar um LB:

![](../media/s-lb-1.png)

Espere ele criar e veja o resultado:

```
kubectl get svc
```

Agora, vamos criar um Server Group.
Em *Containers* selecione o repositório do DockerHub.
Em *LoadBalancer* selecione o LB criado antes.
Troque as portas da aplicação

```
kubectl get pods
```

E agora criamos um pipeline.
Dê um nome para o seu pipeline e adicione um trigger vindo do DockerRegistry:

![](../media/s-trigger.png)

E agora adicione um stage, ao adicionar, não se esqueça de trocar a configuração do container no seu ReplicaSet:

![](../media/tag-runtime.png)

A sua tag deve ser resolvida em tempo de execução para não dar conflito nas builds.

E agora adicione mais um stage para destruir um Server Group:

![](../media/destroy-sg.png)

Salve. Para testar, devemos fazer alguma change no repositório do Github.

