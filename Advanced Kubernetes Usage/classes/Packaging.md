# Packaging with Helm

Helm é um package manager para o K8s. Assim podemos fazer o deploy de aplicações bem rapidamente.

É mantido pelo CNCF - Cloud Native Computing Foundation, junto com o Kubernetes.

Helm usa um formato específico chamado de **charts**.

Um chart é uma coleção de arquivos que descrevem os recursos que o K8s usa.

Um chart pode fazer o deploy de uma app ou de um db. Ou mesmo, podem ter dependências entre si, como um chart de wordpress, precisamos de um chart para o mysql. 

O chart gera yaml que o K8s entende. Ele contém referências a valores que podem ser especificados de chart para chart.

## Install helm

Para instalar o helm:

```
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash
```

Ou podemos achar o código-fonte no [Github](https://github.com/kubernetes/helm).

## Install MySQL with Helm

```
helm init

helm install --name my-mysql --set mysqlRootPassword=secretpassword,mysqlUser=my-user,mysqlPassword=my-password,mysqlDatabase=my-database stable/mysql
```

O init do helm depende de um cluster de K8s rodando. E por default ele vai instalar o Tiller, que é o server-side component do helm.

Após o comando, o Helm nos dá alguns comandos para entrarmos no banco, ou pegar a senha de root e etc.