# Job Resource

Podemos fazer um Pod ser um Job, com uma hora marcada e uma tarefa específica.

Podemos fazer isso com um ReplicationCntroller ou um ReplicaSet, mas isso deixa um Pod sendo reiniciado indefinidamente.

Pra isso temos o objeto Job Resource.

Temos 3 tipos de Jobs:

1. non-parallel
2. parallel
3. parallel with queue

## Non-Parallel

O *Job* resource vai monitorar o job e restartar o pod se ele morrer ou for deletado.
E ainda podemos colocar uma restartPolicy.

Quando o pod terminar o Job, o Job termina.

Um exemplo:

```yaml
kind: Job
apiVersion: batch/v1
metadate:
    name: pi
spec:
    template:
        metadata:
            name: pi
        spec:
            containers:
            - name: pi
              image: perl:latest
              command: ["perl", "-Mbignum=bpi", "-wle", "print bpi(2000)"]
            restartPolicy: Never
```

## Parallel Jobs

Um Job pode usar múltiplos Pods em paralelo, e usamos um *fixed completion* para saber quando termina.

O Job termina quando o *fixed completion* for igual ao número setado. Por exemplo, queremos que 10 Pods cheguem em um determinado resultado, só assim o Job termina.

## Parallel Job with Queue

Precisamos de um software para fazer a coordenação do que cada Pod vai fazer.
Quando qualquer Pod terminar, não há mais Job para ser feito.

Os Pods devem comunicar entre eles para saber quando o Job terminou.

## Running a Job

Vamos criar os recursos que estão em:

```
kubectl create -f resources/jobs/job.yml
```

Para ver o resultado:

```
kubectl get jobs
kubectl describe jobs/pi
```

## Scheduling

Um *CronJob* pode fazer um Job baseado em um horário. Seja num horário específico ou em uma rotina.

É bem parecido com a crontab do Linux.

Podemos ver o formato usado na [Wikipedia](https://pt.wikipedia.org/wiki/Crontab).

Temos um exemplo em *resources/jobs/cronjob.yml*:

```yaml
apiVersion: batch/v2alpha1
kind: CronJob
metadata:
  name: hello
spec:
  schedule: "*/1 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: my-cronjob
            image: busybox
            args:
            - /bin/sh
            - -c
            - echo This commmand runs every minute
          restartPolicy: OnFailure
```

Vamos criar os recursos que estão em:

```
kubectl create -f resources/jobs/cronjob.yml
```

Para ver o resultado:

```
kubectl get cronjob
kubectl describe cronjob/hello
```

Caso seja necessário rodar os jobs em v2alpha1, é necessário habilitar isso no seu cluster. Para isso, edite o arquivo de config do k8s:

```yaml
spec:
  kubeAPIServer:
    runtimeConfig:
      batch/v2alpha1: "true"
```