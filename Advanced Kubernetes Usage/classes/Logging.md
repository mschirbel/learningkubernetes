# Logging

Log são importantes para mostrar o comportamento de uma aplicação. Seja um debug, ou um erro.
E é muito simples visualizar as logs de uma app somente.

No Kubernetes podemos fazer isso com o comando:

```
kubectl logs <pod name>
```

Mas e se tivermos diversos pods e diversos sistemas? Usamos então **Log Agregation*.

E pode ser feito com ferramentas modernas para fazer isso, como a ELK Stack = ElasticSearch + Logstash + Kibana.

No nosso caso usaremos:

1. FluentD - para log fowarding
2. ElastciSearch - para indexar a log
3. Kibana - para ver a log
4. Logtrail - UI para análise de logs

## Demo

Temos os arquivos necessários em *resources/logging/*.

Após criar o cluster, use o comando para criar toda a nossa ELK Stack:

```
kubectl create -f resources/logging/
```

As máquinas precisam ser:

    - No mínimo 3
    - t2.medium(aws) no mínimo

Perceba que no arquivo do ElasticSearch temos um StatefulSet. Isso é usado quando temos um nome estático para o hostname do Cluster. Os pods terão o nome de elasticsearch-logging-X, sendo X como 0,1,2...n.

---

Outro fato interessante, é que o parâmetro **vm.max_map_count** do SO deve ser no mínimo *262144*. Para checar o valor:

```
/sbin/sysctl -w vm.max_map_count
```

---

DaemonSet é um objeto que vai materializar aquela configuração em cada um dos Nods. No nosso caso, o Fluentd deve estar presente em todos os Nods, por isso usamos um DaemonSet.

E isso é feito somente 1 vez.

---

Kibana não precisa de Volumes, pois é somente um frontend para nossa Stack, ou melhor, para o ElasticSearch.

Também não estamos usando dois addons que são pagos:

```yaml
   - name: XPACK_MONITORING_ENABLED
            value: "false"
   - name: XPACK_SECURITY_ENABLED
            value: "false"
```

O problema disso é que não teremos segurança no endpoint.

---

Após criar a Stack, acesse o IP com a porta do Kibana, se for na AWS, use o dns do LB.

Na dashboard, nosso **index name or pattern** será: ```logstash-*```. Mesmo que estamos usando fluentd, ainda assim será indexado com o logstash.

Se você usar a imagem com o LogTrail, o plugin permitirá fazer pesquisas diretamente nas logs do sistema.