# Advanced Kubernetes Usage

[This](https://www.udemy.com/learn-devops-advanced-kubernetes-usage/learn/lecture/7985960#overview) is the course related.

## Steps

1. Introduction
2. Logging
3. Authentication
4. Authorization
5. Package Management
6. Job Resource
7. Scheduling
8. Spinnaker
9. Linkerd
10. Federation
11. Monitoring